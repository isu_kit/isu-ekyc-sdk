package com.iserveu.livenessselfie.databinding;
import com.iserveu.livenessselfie.R;
import com.iserveu.livenessselfie.BR;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class LivenessActivityBindingImpl extends LivenessActivityBinding  {

    @Nullable
    private static final androidx.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.selfie_consent_layout_liveness, 1);
        sViewsWithIds.put(R.id.selfie_consent_image, 2);
        sViewsWithIds.put(R.id.proceed_liveness_selfie_capture_btn, 3);
        sViewsWithIds.put(R.id.liveness_selfie_camera_layout, 4);
        sViewsWithIds.put(R.id.close_liveness_camera, 5);
        sViewsWithIds.put(R.id.mainLayout, 6);
        sViewsWithIds.put(R.id.task_Tv, 7);
        sViewsWithIds.put(R.id.conlay, 8);
        sViewsWithIds.put(R.id.card_view, 9);
        sViewsWithIds.put(R.id.pv, 10);
        sViewsWithIds.put(R.id.pv_border, 11);
        sViewsWithIds.put(R.id.timer_textView, 12);
        sViewsWithIds.put(R.id.textView3, 13);
        sViewsWithIds.put(R.id.newTaskTextView, 14);
        sViewsWithIds.put(R.id.faceMaskCheckBtn, 15);
        sViewsWithIds.put(R.id.imgCapture, 16);
        sViewsWithIds.put(R.id.capture_text_liveness, 17);
        sViewsWithIds.put(R.id.control, 18);
        sViewsWithIds.put(R.id.imgCancel, 19);
        sViewsWithIds.put(R.id.imgDone, 20);
        sViewsWithIds.put(R.id.camera_controller_btn, 21);
    }
    // views
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public LivenessActivityBindingImpl(@Nullable androidx.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 22, sIncludes, sViewsWithIds));
    }
    private LivenessActivityBindingImpl(androidx.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.ImageView) bindings[21]
            , (android.widget.TextView) bindings[17]
            , (androidx.cardview.widget.CardView) bindings[9]
            , (android.widget.ImageView) bindings[5]
            , (android.widget.RelativeLayout) bindings[8]
            , (android.widget.RelativeLayout) bindings[18]
            , (android.widget.Button) bindings[15]
            , (android.widget.ImageView) bindings[19]
            , (android.widget.ImageView) bindings[16]
            , (android.widget.ImageView) bindings[20]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[4]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[6]
            , (android.widget.TextView) bindings[14]
            , (android.widget.Button) bindings[3]
            , (androidx.camera.view.PreviewView) bindings[10]
            , (androidx.camera.view.PreviewView) bindings[11]
            , (androidx.constraintlayout.widget.ConstraintLayout) bindings[0]
            , (android.widget.ImageView) bindings[2]
            , (android.widget.RelativeLayout) bindings[1]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[13]
            , (android.widget.TextView) bindings[12]
            );
        this.rootView.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x1L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
            return variableSet;
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        // batch finished
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): null
    flag mapping end*/
    //end
}