package com.iserveu.livenessselfie;

import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.DataBinderMapper;
import androidx.databinding.DataBindingComponent;
import androidx.databinding.ViewDataBinding;
import com.iserveu.livenessselfie.databinding.ActivityFaceDetectionBindingImpl;
import com.iserveu.livenessselfie.databinding.ActivityFullImageViewBindingImpl;
import com.iserveu.livenessselfie.databinding.LivenessActivityBindingImpl;
import java.lang.IllegalArgumentException;
import java.lang.Integer;
import java.lang.Object;
import java.lang.Override;
import java.lang.RuntimeException;
import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataBinderMapperImpl extends DataBinderMapper {
  private static final int LAYOUT_ACTIVITYFACEDETECTION = 1;

  private static final int LAYOUT_ACTIVITYFULLIMAGEVIEW = 2;

  private static final int LAYOUT_LIVENESSACTIVITY = 3;

  private static final SparseIntArray INTERNAL_LAYOUT_ID_LOOKUP = new SparseIntArray(3);

  static {
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.iserveu.livenessselfie.R.layout.activity_face_detection, LAYOUT_ACTIVITYFACEDETECTION);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.iserveu.livenessselfie.R.layout.activity_full_image_view, LAYOUT_ACTIVITYFULLIMAGEVIEW);
    INTERNAL_LAYOUT_ID_LOOKUP.put(com.iserveu.livenessselfie.R.layout.liveness_activity, LAYOUT_LIVENESSACTIVITY);
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View view, int layoutId) {
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = view.getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
        case  LAYOUT_ACTIVITYFACEDETECTION: {
          if ("layout-v26/activity_face_detection_0".equals(tag)) {
            return new ActivityFaceDetectionBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_face_detection is invalid. Received: " + tag);
        }
        case  LAYOUT_ACTIVITYFULLIMAGEVIEW: {
          if ("layout-v26/activity_full_image_view_0".equals(tag)) {
            return new ActivityFullImageViewBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for activity_full_image_view is invalid. Received: " + tag);
        }
        case  LAYOUT_LIVENESSACTIVITY: {
          if ("layout/liveness_activity_0".equals(tag)) {
            return new LivenessActivityBindingImpl(component, view);
          }
          throw new IllegalArgumentException("The tag for liveness_activity is invalid. Received: " + tag);
        }
      }
    }
    return null;
  }

  @Override
  public ViewDataBinding getDataBinder(DataBindingComponent component, View[] views, int layoutId) {
    if(views == null || views.length == 0) {
      return null;
    }
    int localizedLayoutId = INTERNAL_LAYOUT_ID_LOOKUP.get(layoutId);
    if(localizedLayoutId > 0) {
      final Object tag = views[0].getTag();
      if(tag == null) {
        throw new RuntimeException("view must have a tag");
      }
      switch(localizedLayoutId) {
      }
    }
    return null;
  }

  @Override
  public int getLayoutId(String tag) {
    if (tag == null) {
      return 0;
    }
    Integer tmpVal = InnerLayoutIdLookup.sKeys.get(tag);
    return tmpVal == null ? 0 : tmpVal;
  }

  @Override
  public String convertBrIdToString(int localId) {
    String tmpVal = InnerBrLookup.sKeys.get(localId);
    return tmpVal;
  }

  @Override
  public List<DataBinderMapper> collectDependencies() {
    ArrayList<DataBinderMapper> result = new ArrayList<DataBinderMapper>(1);
    result.add(new androidx.databinding.library.baseAdapters.DataBinderMapperImpl());
    return result;
  }

  private static class InnerBrLookup {
    static final SparseArray<String> sKeys = new SparseArray<String>(1);

    static {
      sKeys.put(0, "_all");
    }
  }

  private static class InnerLayoutIdLookup {
    static final HashMap<String, Integer> sKeys = new HashMap<String, Integer>(3);

    static {
      sKeys.put("layout-v26/activity_face_detection_0", com.iserveu.livenessselfie.R.layout.activity_face_detection);
      sKeys.put("layout-v26/activity_full_image_view_0", com.iserveu.livenessselfie.R.layout.activity_full_image_view);
      sKeys.put("layout/liveness_activity_0", com.iserveu.livenessselfie.R.layout.liveness_activity);
    }
  }
}
