package com.iserveu.isuekyc.aadhaar_front;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.databinding.ActivityAadhaarFrontConsentBinding;

public class AadhaarFrontConsentActivity extends AppCompatActivity {
    private ActivityAadhaarFrontConsentBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAadhaarFrontConsentBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


    }
}