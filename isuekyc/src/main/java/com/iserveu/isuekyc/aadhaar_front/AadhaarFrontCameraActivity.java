package com.iserveu.isuekyc.aadhaar_front;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.core.ResolutionInfo;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.common.util.concurrent.ListenableFuture;
import com.iserveu.isuekyc.ActivityHome;
import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.aadhaar_back.AadhaarBackCameraActivity;
import com.iserveu.isuekyc.databinding.ActivityAadhaarFrontCameraBinding;
import com.iserveu.isuekyc.dialogs.DialogFactory;
import com.iserveu.isuekyc.intent.IntentFactory;
import com.iserveu.isuekyc.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import co.hyperverge.hypersnapsdk.HyperSnapSDK;
import co.hyperverge.hypersnapsdk.objects.HyperSnapParams;

public class AadhaarFrontCameraActivity extends AppCompatActivity {

    private ActivityAadhaarFrontCameraBinding binding;
    private static final String TAG = AadhaarFrontCameraActivity.class.getSimpleName();
    private Executor executor = Executors.newSingleThreadExecutor();
    private int REQUEST_CODE_PERMISSIONS = 1001;
    private  final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};
    public static Bitmap capturedBitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAadhaarFrontCameraBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
//        getSupportActionBar().hide();
        binding.proceedAfCaptureBtn.setOnClickListener(view -> {
            binding.aadhaarFrontConsentLayout.setVisibility(View.GONE);
            binding.aadhaarFcLayout.setVisibility(View.VISIBLE);
        });

        binding.closeCamera.setOnClickListener(view -> {
            finishAffinity();
        });

        if (!Utils.isAllPermissionsGranted(this,REQUIRED_PERMISSIONS)){
            ActivityCompat.requestPermissions(this,REQUIRED_PERMISSIONS,REQUEST_CODE_PERMISSIONS);
        }else{

            startCamera();
        }




    }






    private void startCamera() {

        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() {
                try {

                    ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                    bindPreview(cameraProvider);

                } catch (ExecutionException | InterruptedException e) {
                    // No errors need to be handled for this Future.
                    // This should never be reached.
                }
            }
        }, ContextCompat.getMainExecutor(this));
    }

    void bindPreview(@NonNull ProcessCameraProvider cameraProvider) {

        Preview preview = new Preview.Builder()
//                .setTargetAspectRatio(AspectRatio.RATIO_16_9)
                .build();

        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();


        ImageAnalysis imageAnalysis = new ImageAnalysis.Builder()
                .build();


        ImageCapture.Builder builder = new ImageCapture.Builder();


        final ImageCapture imageCapture = builder
                .setTargetRotation(this.getWindowManager().getDefaultDisplay().getRotation())
//                .setTargetResolution(new Size(1080, 1920))
//                .setTargetAspectRatio(AspectRatio.RATIO_16_9)
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                .build();



        preview.setSurfaceProvider(binding.aadhaarFrontCameraPreviewView.getSurfaceProvider());

        Camera camera = cameraProvider.bindToLifecycle((LifecycleOwner)this, cameraSelector, preview, imageAnalysis, imageCapture);
       /* CameraControl cameraControl = camera.getCameraControl();
        cameraControl.startFocusAndMetering().;*/
        binding.cameraBubble.setOnClickListener(v -> {
            capturedBitmap = null;

            DialogFactory.showImageCaptureInProgressDialog(this,"Wait...");
            closeCamera();
            SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
            long timestamp = System.currentTimeMillis();

            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, timestamp);
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");


            ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(
                    getContentResolver(),
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    contentValues).build();

               /* capturedBitmap = binding.aadhaarFrontCameraPreviewView.getBitmap();
            if (capturedBitmap!=null){
                startActivity(IntentFactory.returnAadhaarFrontImageReviewActivity(this));
            }*/


//                 getBitmapFromView(binding.aadhaarFrontCameraLayout);





            imageCapture.takePicture(
                    outputFileOptions, executor, new ImageCapture.OnImageSavedCallback () {
                        @Override
                        public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    ResolutionInfo info =  imageAnalysis.getResolutionInfo();
                                    if (info!=null){
                                        Log.d(TAG, "run: resolution "+info.getResolution()+" "+info.getRotationDegrees());
                                    }else{
                                        Log.d(TAG, "run: resolution is null");
                                    }
                                    Log.d("filePath", "run: "+outputFileResults.getSavedUri());
                                    Uri imageUri = outputFileResults.getSavedUri();
//                            Toast.makeText(MainActivity.this, "Image Saved successfully", Toast.LENGTH_SHORT).show();
                                    Log.d(TAG, "run: Image saved Successfully");
                                    if (imageUri!=null){
                                        startActivity(IntentFactory.returnAadhaarFrontImageReviewActivity(AadhaarFrontCameraActivity.this).putExtra("imageUri",imageUri.toString()));
                                    }else{
                                        DialogFactory.dismissCameraProgressDialog();
                                        Toast.makeText(AadhaarFrontCameraActivity.this, "Failed to capture image", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                        @Override
                        public void onError(@NonNull ImageCaptureException error) {
                            error.printStackTrace();
                        }
                    });
        });
        if(!AadhaarFrontCameraActivity.this.isDestroyed()) {
            DialogFactory.dismissCameraProgressDialog();
        }
    }

    private void closeCamera() {
        binding.aadhaarFrontCameraPreviewView.setVisibility(View.GONE);

    }

    private void openCamera() {
        binding.aadhaarFrontCameraPreviewView.setVisibility(View.VISIBLE);
    }
    @Override
    protected void onResume() {
        super.onResume();
        openCamera();
    }

    @Override
    protected void onDestroy() {
        DialogFactory.dismissCameraProgressDialog();
        super.onDestroy();
    }

    private void getBitmapFromView(View view){
        capturedBitmap = null;
        view.setDrawingCacheEnabled(true);
        view.setBackgroundColor(getResources().getColor(R.color.white));
        capturedBitmap = Bitmap.createBitmap(view.getDrawingCache());
        view.setDrawingCacheEnabled(false);
       if (capturedBitmap!=null){
           startActivity(IntentFactory.returnAadhaarFrontImageReviewActivity(this));
       }
    }

    private void sendEncodeImage( Bitmap bitmap) {
        String encodedImage;
        try {


            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            encodedImage = Base64.encodeToString(byteArray, Base64.DEFAULT);
            Log.d(TAG, "encodeDetectedSignature: "+encodedImage);



        } catch (Exception e) {
            e.printStackTrace();
            encodedImage = "";
            Toast.makeText(this, "Please put full image inside camera view ", Toast.LENGTH_SHORT).show();
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSIONS){
            if (Utils.isAllPermissionsGranted(this,REQUIRED_PERMISSIONS)) {
                startCamera();
            } else {
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show();
                this.finish();
            }
        }
    }
}