package com.iserveu.isuekyc.aadhaar_front;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.databinding.ActivityAadhaarFrontDataBinding;

public class AadhaarFrontDataActivity extends AppCompatActivity {
    private static final String TAG = AadhaarFrontDataActivity.class.getSimpleName();
    private ActivityAadhaarFrontDataBinding binding;
    private int totalScore =0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAadhaarFrontDataBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        showAadhaarFrontPageData();
    }

    private void showAadhaarFrontPageData() {
        {
            String vid,gender,dob,name,aadhar;
            if (getIntent().hasExtra("VID")){
                vid = getIntent().getStringExtra("VID");
                if (!vid.equals("")){
                    totalScore+=20;
                }
            }else{
                vid="";
            }
            if (getIntent().hasExtra("gender")){
                gender = getIntent().getStringExtra("gender");
                totalScore+=20;

            }else{
                gender="";
            }
            if (getIntent().hasExtra("DOB")){
                dob = getIntent().getStringExtra("DOB");
                totalScore+=20;

            }else{
                dob="";
            }
            if (getIntent().hasExtra("Name")){
                name = getIntent().getStringExtra("Name");
                totalScore+=20;

            }else{
                name="";
            }
            if (getIntent().hasExtra("aadhar")){
                aadhar = getIntent().getStringExtra("aadhar");
                totalScore+=20;

            }else{
                aadhar="";
            }


            binding.aadharFrontPage.setVisibility(View.VISIBLE);


            if (aadhar!=null){
                binding.etAadharNo.setText(aadhar.trim());

            }
            if (name!=null){
                binding.etName.setText(name);

            }
            if (aadhar!=null){
                binding.etAadharNo.setText(aadhar);

            }
            if (dob!=null){
                binding.etDob.setText(dob);
            }
            if (vid != null) {
                binding.etVid.setText(vid.trim());
            }

            if (gender!=null){
                binding.edGender.setText(gender.trim());
            }

            Log.d(TAG, "onCreate: accuracyScore "+totalScore);
            binding.scoreTextView.setText(" Accuracy Score : "+totalScore+"%");

        }
    }
}