package com.iserveu.isuekyc.aadhaar_back;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.databinding.ActivityAadhaarBackDataBinding;
import com.iserveu.isuekyc.databinding.ActivityAadhaarFrontDataBinding;

public class AadhaarBackDataActivity extends AppCompatActivity {
    private static final String TAG = AadhaarBackDataActivity.class.getSimpleName();
    private ActivityAadhaarBackDataBinding binding;
    private int totalScore =0;
    private String vid="",address="",state="",pincode="",aadhar="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAadhaarBackDataBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        showAadhaarFrontPageData();
    }

    private void showAadhaarFrontPageData() {

        if (getIntent().hasExtra("VID")){
            vid = getIntent().getStringExtra("VID");
            if (!vid.equals("")){
                totalScore+=20;
            }
        }else{
            vid="";
        }



        if (getIntent().hasExtra("aadhar")){
            aadhar = getIntent().getStringExtra("aadhar");
            totalScore+=20;

        }else{
            aadhar="";
        }

        if (getIntent().hasExtra("address")){
            address = getIntent().getStringExtra("address");
            totalScore+=20;

        }else{
            address="";
        }

        if (getIntent().hasExtra("state")){
            state = getIntent().getStringExtra("state");
            totalScore+=20;

        }else{
            state="";
        }

        if (getIntent().hasExtra("pincode")){
            pincode = getIntent().getStringExtra("pincode");
            totalScore+=20;

        }else{
            pincode="";
        }
        binding.aadharBackPage.setVisibility(View.VISIBLE);
        if (aadhar!=null){
            binding.etAadharNo1.setText(aadhar.trim());
        }
        if (vid != null) {
            binding.edVid1.setText(vid.trim());
        }

        if (address!=null){
            binding.etAddress.setText(address.trim());
        }
        if (state!=null){
            binding.etState.setText(state);
        }
        if (pincode!=null){
            binding.etPinCode.setText(pincode);
        }
        Log.d(TAG, "onCreate: accuracyScore "+totalScore);
        binding.scoreTextView.setText(" Accuracy Score : "+totalScore+"%");


    }
}