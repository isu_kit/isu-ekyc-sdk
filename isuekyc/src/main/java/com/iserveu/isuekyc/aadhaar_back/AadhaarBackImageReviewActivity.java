package com.iserveu.isuekyc.aadhaar_back;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import com.iserveu.isuekyc.ActivityHome;
import com.iserveu.isuekyc.Constants;
import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.databinding.ActivityAadhaarBackImageReviewBinding;
import com.iserveu.isuekyc.intent.IntentFactory;

import java.io.IOException;

public class AadhaarBackImageReviewActivity extends AppCompatActivity {

    private ActivityAadhaarBackImageReviewBinding binding;
    private Bitmap mSelectedImage,rotatedBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAadhaarBackImageReviewBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        getSupportActionBar().hide();
        initViews();
        showCapturedImage();
    }

    private void showCapturedImage() {
        Uri imageUri = Uri.parse(getIntent().getExtras().getString("imageUri"));
        Constants.backImageUri = imageUri.toString();
        try {
            mSelectedImage = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
//            mSelectedImage = AadhaarBackCameraActivity.capturedBitmap;
            Matrix rotationMatrix = new Matrix();
            if(mSelectedImage.getWidth() >= mSelectedImage.getHeight()){
                rotationMatrix.setRotate(90);
            }else{
                rotationMatrix.setRotate(0);
            }

            rotatedBitmap = Bitmap.createBitmap(mSelectedImage,0,0,mSelectedImage.getWidth(),mSelectedImage.getHeight(),rotationMatrix,true);
            binding.reviewImage.setImageBitmap(rotatedBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, ActivityHome.class);
        startActivity(intent);
//        startActivity(IntentFactory.returnActivityHomeActivity(AadhaarFrontCameraActivity.this));
    }

    private void initViews() {
        binding.confirmButton.setOnClickListener(view -> {
            AadhaarBackImageProcessor.getInstance().processAadhaarBackImage(this,rotatedBitmap);
        });
        binding.retakeButton.setOnClickListener(view -> {
            startActivity(IntentFactory.returnAppMainActivity(AadhaarBackImageReviewActivity.this));
        });
    }
}