package com.iserveu.isuekyc.aadhaar_back;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.Camera;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageCapture;
import androidx.camera.core.ImageCaptureException;
import androidx.camera.core.Preview;
import androidx.camera.core.ResolutionInfo;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LifecycleOwner;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.common.util.concurrent.ListenableFuture;
import com.iserveu.isuekyc.ActivityHome;
import com.iserveu.isuekyc.aadhaar_front.AadhaarFrontCameraActivity;
import com.iserveu.isuekyc.databinding.ActivityAadhaarBackCameraBinding;
import com.iserveu.isuekyc.dialogs.DialogFactory;
import com.iserveu.isuekyc.intent.IntentFactory;
import com.iserveu.isuekyc.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class AadhaarBackCameraActivity extends AppCompatActivity {

    private ActivityAadhaarBackCameraBinding binding;
    private static final String TAG = AadhaarFrontCameraActivity.class.getSimpleName();
    private Executor executor = Executors.newSingleThreadExecutor();
    private int REQUEST_CODE_PERMISSIONS = 1001;
    private  final String[] REQUIRED_PERMISSIONS = new String[]{"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE"};
    public static Bitmap capturedBitmap;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityAadhaarBackCameraBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        getSupportActionBar().hide();
        binding.proceedAbCaptureBtn.setOnClickListener(view -> {
            binding.aadhaarBackConsentLayout.setVisibility(View.GONE);
            binding.aadhaarBcLayout.setVisibility(View.VISIBLE);
        });

        binding.closeCamera.setOnClickListener(view -> {
            finishAffinity();
        });

        if (!Utils.isAllPermissionsGranted(this,REQUIRED_PERMISSIONS)){
            ActivityCompat.requestPermissions(this,REQUIRED_PERMISSIONS,REQUEST_CODE_PERMISSIONS);
        }else{

            startCamera();
        }
    }



    private void startCamera() {

        final ListenableFuture<ProcessCameraProvider> cameraProviderFuture = ProcessCameraProvider.getInstance(this);

        cameraProviderFuture.addListener(new Runnable() {
            @Override
            public void run() {
                try {

                    ProcessCameraProvider cameraProvider = cameraProviderFuture.get();
                    bindPreview(cameraProvider);

                } catch (ExecutionException | InterruptedException e) {
                    // No errors need to be handled for this Future.
                    // This should never be reached.
                }
            }
        }, ContextCompat.getMainExecutor(this));
    }

    void bindPreview(@NonNull ProcessCameraProvider cameraProvider) {

        Preview preview = new Preview.Builder()
//                .setTargetAspectRatio(AspectRatio.RATIO_16_9)
                .build();

        CameraSelector cameraSelector = new CameraSelector.Builder()
                .requireLensFacing(CameraSelector.LENS_FACING_BACK)
                .build();


        ImageAnalysis imageAnalysis = new ImageAnalysis.Builder()
                .build();


        ImageCapture.Builder builder = new ImageCapture.Builder();


        final ImageCapture imageCapture = builder
                .setTargetRotation(this.getWindowManager().getDefaultDisplay().getRotation())
//                .setTargetResolution(new Size(1080, 1920))
//                .setTargetAspectRatio(AspectRatio.RATIO_16_9)
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                .build();



        preview.setSurfaceProvider(binding.aadhaarBackCameraPreviewView.getSurfaceProvider());

        Camera camera = cameraProvider.bindToLifecycle((LifecycleOwner)this, cameraSelector, preview, imageAnalysis, imageCapture);
       /* CameraControl cameraControl = camera.getCameraControl();
        cameraControl.startFocusAndMetering().;*/
        binding.cameraBubble.setOnClickListener(v -> {

            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
            capturedBitmap = null;
            closeCamera();
//            DialogFactory.showImageCaptureInProgressDialog(this,"Wait...");
            SimpleDateFormat mDateFormat = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
            long timestamp = System.currentTimeMillis();

            ContentValues contentValues = new ContentValues();
            contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, timestamp);
            contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg");


            ImageCapture.OutputFileOptions outputFileOptions = new ImageCapture.OutputFileOptions.Builder(
                    getContentResolver(),
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    contentValues).build();


            /*capturedBitmap = binding.aadhaarBackCameraPreviewView.getBitmap();
            if (capturedBitmap!=null){
                startActivity(IntentFactory.returnAadhaarBackImageReviewActivity(this));
            }*/
            imageCapture.takePicture(
                    outputFileOptions, executor, new ImageCapture.OnImageSavedCallback () {
                        @Override
                        public void onImageSaved(@NonNull ImageCapture.OutputFileResults outputFileResults) {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    ResolutionInfo info =  imageAnalysis.getResolutionInfo();
                                    if (info!=null){
                                        Log.d(TAG, "run: resolution "+info.getResolution()+" "+info.getRotationDegrees());
                                    }else{
                                        Log.d(TAG, "run: resolution is null");
                                    }
                                    Log.d("filePath", "run: "+outputFileResults.getSavedUri());
                                    Uri imageUri = outputFileResults.getSavedUri();
//                            Toast.makeText(MainActivity.this, "Image Saved successfully", Toast.LENGTH_SHORT).show();
                                    Log.d(TAG, "run: Image saved Successfully");


//                                    startActivity(new Intent(IntentFactory.returnAadhaarBackImageReviewActivity()).putExtra(Constants.IMAGE_TYPE,imageTypeGlobal).putExtra("imageUri",imageUri.toString()));'
                                    if (imageUri!=null){
                                        startActivity(IntentFactory.returnAadhaarBackImageReviewActivity(AadhaarBackCameraActivity.this).putExtra("imageUri",imageUri.toString()));
                                    }else{
                                        DialogFactory.dismissCameraProgressDialog();
                                        Toast.makeText(AadhaarBackCameraActivity.this, "Failed to capture image", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                        @Override
                        public void onError(@NonNull ImageCaptureException error) {
                            error.printStackTrace();
                            dismissProgressDialog();
                        }
                    });
        });
        DialogFactory.dismissCameraProgressDialog();
    }

    private void closeCamera() {
        binding.aadhaarBackCameraPreviewView.setVisibility(View.GONE);

    }

    private void openCamera() {
        binding.aadhaarBackCameraPreviewView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSIONS){
            if (Utils.isAllPermissionsGranted(this,REQUIRED_PERMISSIONS)) {
                startCamera();
            } else {
                Toast.makeText(this, "Permissions not granted by the user.", Toast.LENGTH_SHORT).show();
                this.finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        dismissProgressDialog();
        openCamera();
    }


    private void dismissProgressDialog(){
        if (progressDialog!=null){
            progressDialog.dismiss();
        }
    }
}