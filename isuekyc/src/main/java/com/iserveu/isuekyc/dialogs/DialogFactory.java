package com.iserveu.isuekyc.dialogs;

import android.app.ProgressDialog;
import android.content.Context;

public class DialogFactory {
    private static ProgressDialog progressDialog;
    private static ProgressDialog progressDialogCameraActivity;

    public static void showImageProcessingDialog(Context context,String message){
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void showImageCaptureInProgressDialog(Context context, String message){
        progressDialogCameraActivity = new ProgressDialog(context);
        progressDialogCameraActivity.setMessage(message);
        progressDialogCameraActivity.setCancelable(false);
        progressDialogCameraActivity.show();
    }

    public static void showImageProcessingDialog(Context context,String title,String message){
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    public static void dismissDialog(){
        if (progressDialog!=null){
            progressDialog.dismiss();
        }
    }

    public static void dismissCameraProgressDialog(){
        if (progressDialogCameraActivity!=null){
            progressDialogCameraActivity.dismiss();
        }
    }
}
