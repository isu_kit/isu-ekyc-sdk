package com.iserveu.isuekyc.pan_card;

public class PanCardModelISUeKyc {
    private static PanCardModelISUeKyc panCardModelISUeKyc;
    private String panNo;
    private String panName;
    private String panFatherName;
    private String panBOB;
    private String panEncodedSignature;
    private String panEncodedUserImage;
    private String panMotherName;
    private boolean isMotherNameFound;
    private String panImagUrl;


    public String getPanImagUrl() {
        return panImagUrl;
    }

    public void setPanImagUrl(String panImagUrl) {
        this.panImagUrl = panImagUrl;
    }





    public static PanCardModelISUeKyc getInstance(){
        if (panCardModelISUeKyc==null){
            panCardModelISUeKyc = new PanCardModelISUeKyc();
        }
        return panCardModelISUeKyc;
    }

    public String getPanNo() {
        return panNo;
    }

    public PanCardModelISUeKyc setPanNo(String panNo) {
        this.panNo = panNo;
        return panCardModelISUeKyc;
    }

    public String getPanName() {
        return panName;
    }

    public PanCardModelISUeKyc setPanName(String panName) {
        this.panName = panName;
        return panCardModelISUeKyc;
    }

    public String getPanFatherName() {
        return panFatherName;
    }

    public PanCardModelISUeKyc setPanFatherName(String panFatherName) {
        this.panFatherName = panFatherName;
        return panCardModelISUeKyc;

    }

    public String getPanBOB() {
        return panBOB;
    }

    public PanCardModelISUeKyc setPanBOB(String panBOB) {
        this.panBOB = panBOB;
        return panCardModelISUeKyc;

    }

    public String getPanEncodedSignature() {
        return panEncodedSignature;
    }

    public PanCardModelISUeKyc setPanEncodedSignature(String panEncodedSignature) {
        this.panEncodedSignature = panEncodedSignature;
        return panCardModelISUeKyc;

    }

    public String getPanEncodedUserImage() {
        return panEncodedUserImage;
    }

    public PanCardModelISUeKyc setPanEncodedUserImage(String panEncodedUserImage) {
        this.panEncodedUserImage = panEncodedUserImage;
        return panCardModelISUeKyc;

    }

    public String getPanMotherName() {
        return panMotherName;
    }

    public PanCardModelISUeKyc setPanMotherName(String panMotherName) {
        this.panMotherName = panMotherName;
        return panCardModelISUeKyc;

    }

    public boolean isMotherNameFound() {
        return isMotherNameFound;
    }

    public PanCardModelISUeKyc setMotherNameFound(boolean motherNameFound) {
        isMotherNameFound = motherNameFound;
        return panCardModelISUeKyc;

    }
}
