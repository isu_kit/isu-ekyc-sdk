package com.iserveu.isuekyc.pan_card;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.CountDownTimer;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.latin.TextRecognizerOptions;
import com.iserveu.isuekyc.ActivityHome;
import com.iserveu.isuekyc.Constants;
import com.iserveu.isuekyc.bitmap.BitmapHolder;
import com.iserveu.isuekyc.intent.IntentFactory;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.util.List;

public class PanImageProcessor {
    private static final String TAG = PanImageProcessor.class.getSimpleName();
    private Context context;
    private Bitmap bitmap;
    private static PanImageProcessor panImageProcessor;
    private boolean isPanCardImage = false, isMotherName =false;
    private String panName="",panDOB="",panFatherName="",panNo="",panMotherName="", signatureEncoded ="no", userImageEncoded ="no";
    private int panCardType=0;
    private ProgressDialog progressDialog;
    private Text panTexts;
    private boolean isUserImageSaved= false,isUserSignatureSaved = false;

    public static PanImageProcessor getInstance() {
        panImageProcessor = new PanImageProcessor();
        return panImageProcessor;
    }

    public void processPanImage(Context mContext, Bitmap panImage) {
        context = mContext;
        bitmap = panImage;
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Wait...");
        progressDialog.show();
        if (panImage != null) {
            runTextRecognition();

        }
    }


    private void runTextRecognition() {
        InputImage image = InputImage.fromBitmap(bitmap, 0);

        TextRecognizer recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS);
//        binding.confirmButton.setEnabled(false);
        recognizer.process(image)
                .addOnSuccessListener(
                        new OnSuccessListener<Text>() {
                            @Override
                            public void onSuccess(Text texts) {
//                                binding.confirmButton.setEnabled(true);
                                panTexts = texts;
                                processPanImageTextRecognitionResult(texts);
                                Log.d(TAG, "onSuccess: success");
                            }
                        })
                .addOnFailureListener(
                        new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                // Task failed with an exception
//                                binding.confirmButton.setEnabled(true);
                                Log.d(TAG, "onFailure: "+e.getLocalizedMessage());
                                e.printStackTrace();
                            }
                        });
    }

    private void processPanImageTextRecognitionResult(Text texts) {
        Log.d("complete_text", "Result: " + texts.getText());
        String allText = texts.getText();
        if (allText.contains("Birth") || allText.contains("Name")|| allText.contains("Date") || allText.contains("Card")){
            panCardType =2;
        }else{
            panCardType=1;
        }

        List<Text.TextBlock> blocks = texts.getTextBlocks();
        for (int i=0;i<blocks.size();i++){
            if (blocks.get(i).getText().contains("INCOME") || blocks.get(i).getText().contains("TAX")){
                isPanCardImage = true;
            }

            if (blocks.get(i).getText().contains("Father") || blocks.get(i).getText().contains("Father's")){
                isMotherName = false;
            }

            if (blocks.get(i).getText().contains("Mother") || blocks.get(i).getText().contains("Mother's")){
                isMotherName = true;
            }


        }
        Log.d(TAG, "processPanImageTextRecognitionResult: panCardType "+panCardType);

        if (isPanCardImage){
            try {
                if (blocks.size() == 0) {
                    Toast.makeText(context, "No text found", Toast.LENGTH_SHORT).show();
                    return;
                }
//                FaceDetectionProcessor.getInstance().detectFace(context,bitmap);
                Log.d(TAG, "processPanImageTextRecognitionResult: isMotherName "+isMotherName);


                if (panCardType==2){

                    for (int i=0;i<blocks.size();i++){
                        String blockText = blocks.get(i).getText();
                        Log.d(TAG, "processPanImageTextRecognitionResult: Blocks :: "+i+" "+blockText);



                       /* if (blockText.contains("INCOME") || blockText.contains("TAX")){
                            if (blocks.get(i+1).getText().contains("GOVT") || blocks.get(i+1).getText().contains("INDIA")  || blocks.get(i+1).getText().contains("IND")){
                                panName = blocks.get(i+2).getText();
                                panFatherName = blocks.get(i+3).getText();
                            }

                        }

                        if (blocks.get(i).getText().contains("GOVT") || blocks.get(i).getText().contains("INDIA") || blocks.get(i).getText().contains("IND") || blocks.get(i).getText().contains("OF")){
                            panName = blocks.get(i+1).getText();
                            panFatherName = blocks.get(i+2).getText();
                        }*/

                        if (isDOB(blockText)){
                            panDOB = blockText;
                        }

                        if (blockText.contains("Permanent")){


                            String nextWord  = blocks.get(i+1).getText();
                            panNo = nextWord;
                            Log.d(TAG, "processPanImageTextRecognitionResult: panNo "+panNo);

                        }




                        List<Text.Line> lines = blocks.get(i).getLines();
                        for (int j=0;j<lines.size();j++){
                            String currText = lines.get(j).getText();
                            Log.d(TAG, "processPanImageTextRecognitionResult: lines :: "+j+" "+currText);
                            if (currText.contains("/Name")){
//                     if (lines.size()>=2){
                                panName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panName "+panName);

//                     }
                            }
                            if (currText.contains("/ Name")){
//                     if (lines.size()>=2){
                                panName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panName "+panName);

//                     }
                            }
                            if (currText.contains("Name")){

                                if (!currText.contains("Father") && !currText.contains("Father's") || !currText.contains("ther")){
                                    panName = lines.get(j+1).getText();
                                    Log.d(TAG, "processPanImageTextRecognitionResult: panName "+panName);
                                }
                            }
                      /*  if (currText.contains("Name")){
                            if (!currText.contains("Father")){
                                panName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panName "+panName);
                            }
                        }*/

                        /*if (currText.contains("Name")){
                            if (!currText.contains("Mother")){
                                panName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panName "+panName);
                            }
                        }*/

                            if (currText.contains("Father's")){
//                    if (lines.size()>=2){
                                panFatherName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panFatherName "+panFatherName);
//                    }
                            }
                            if (currText.contains("Father")){
//                    if (lines.size()>=2){
                                panFatherName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panFatherName "+panFatherName);

//                    }
                            }

                            if (currText.contains("Mother's")){
//                    if (lines.size()>=2){
                                panMotherName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panMotherName "+panMotherName);
//                    }
                            }
                            if (currText.contains("Mother")){
//                    if (lines.size()>=2){
                                panMotherName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panMotherName "+panMotherName);

//                    }
                            }

                            if (currText.contains("Birth")){
                                if (lines.size()>=2){
                                    panDOB = lines.get(j+1).getText();
                                    Log.d(TAG, "processPanImageTextRecognitionResult: panDOB "+panDOB);

                                }
                            }
                            if (currText.contains("Date")){
                                if (lines.size()>=2){
                                    panDOB = lines.get(j+1).getText();
                                    Log.d(TAG, "processPanImageTextRecognitionResult: panDOB "+panDOB);

                                }
                            }
                        }


                    }
                }

                if (panCardType==1){
                    for (int i=0;i<blocks.size();i++){
                        String blockText = blocks.get(i).getText();
                        Log.d(TAG, "processPanImageTextRecognitionResult: Blocks :: "+i+" "+blockText);



                        if (blockText.contains("INCOME") || blockText.contains("TAX")){
                            if (blocks.get(i+1).getText().contains("GOVT") || blocks.get(i+1).getText().contains("INDIA")  || blocks.get(i+1).getText().contains("IND")){
                                panName = blocks.get(i+2).getText();
                                panFatherName = blocks.get(i+3).getText();
                            }

                        }

                        if (blocks.get(i).getText().contains("GOVT") || blocks.get(i).getText().contains("INDIA") || blocks.get(i).getText().contains("IND") || blocks.get(i).getText().contains("OF")){
                            panName = blocks.get(i+1).getText();
                            panFatherName = blocks.get(i+2).getText();
                        }

                        if (isDOB(blockText)){
                            panDOB = blockText;
                        }

                        if (blockText.contains("Permanent")){


                            String nextWord  = blocks.get(i+1).getText();
                            panNo = nextWord;
                            Log.d(TAG, "processPanImageTextRecognitionResult: panNo "+panNo);

                        }




                        List<Text.Line> lines = blocks.get(i).getLines();
                        for (int j=0;j<lines.size();j++){
                            String currText = lines.get(j).getText();
                            Log.d(TAG, "processPanImageTextRecognitionResult: lines :: "+j+" "+currText);
                            if (currText.contains("/Name")){
//                     if (lines.size()>=2){
                                panName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panName "+panName);

//                     }
                            }
                            if (currText.contains("/ Name")){
//                     if (lines.size()>=2){
                                panName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panName "+panName);

//                     }
                            }
                      /*  if (currText.contains("Name")){
                            if (!currText.contains("Father")){
                                panName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panName "+panName);
                            }
                        }*/

                        /*if (currText.contains("Name")){
                            if (!currText.contains("Mother")){
                                panName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panName "+panName);
                            }
                        }*/

                            if (currText.contains("Father's")){
//                    if (lines.size()>=2){
                                panFatherName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panFatherName "+panFatherName);
//                    }
                            }
                            if (currText.contains("Father")){
//                    if (lines.size()>=2){
                                panFatherName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panFatherName "+panFatherName);

//                    }
                            }

                            if (currText.contains("Mother's")){
//                    if (lines.size()>=2){
                                panMotherName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panMotherName "+panMotherName);
//                    }
                            }
                            if (currText.contains("Mother")){
//                    if (lines.size()>=2){
                                panMotherName = lines.get(j+1).getText();
                                Log.d(TAG, "processPanImageTextRecognitionResult: panMotherName "+panMotherName);

//                    }
                            }

                            if (currText.contains("Birth")){
                                if (lines.size()>=2){
                                    panDOB = lines.get(j+1).getText();
                                    Log.d(TAG, "processPanImageTextRecognitionResult: panDOB "+panDOB);

                                }
                            }
                            if (currText.contains("Date")){
                                if (lines.size()>=2){
                                    panDOB = lines.get(j+1).getText();
                                    Log.d(TAG, "processPanImageTextRecognitionResult: panDOB "+panDOB);

                                }
                            }
                        }


                    }

                }

                CountDownTimer signTimer = new CountDownTimer(1000,500) {
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        detectSignature(panTexts,bitmap);
                    }
                };
                signTimer.start();


                Log.d(TAG, "processPanImageTextRecognitionResult: panDetails :: "+panNo +" "+panName+" "+panFatherName+" "+panDOB);
            }catch (Exception exception){
                Log.d(TAG, "processPanImageTextRecognitionResult: panDetails :: "+panNo +" "+panName+" "+panFatherName+" "+panDOB);
                Log.d(TAG, "processPanImageTextRecognitionResult: "+exception.getLocalizedMessage());
            }
        }else{
            dismissProgressDialog();
            Toast.makeText(context, "Please Capture Pan Card Image", Toast.LENGTH_SHORT).show();
        }

    }

    private void validateAndSendData() {
        if (signatureEncoded.equalsIgnoreCase("no")){
            detectSignature(panTexts,bitmap);
        }

        if (userImageEncoded.equalsIgnoreCase("no")){
            detectFace(context,bitmap);
        }




        PanCardModelISUeKyc.getInstance().setPanNo(panNo)
                .setPanBOB(panDOB).setPanName(panName)
                .setMotherNameFound(isMotherName).setPanMotherName(panMotherName)
                .setPanFatherName(panFatherName).setPanEncodedSignature(signatureEncoded)
                .setPanEncodedUserImage(userImageEncoded)
                .setPanImagUrl(Constants.panImageUri);


//        Intent intent =IntentFactory.returnAppMainActivity(context).putExtra("imgType", "panCard");
//        context.startActivity(intent);
        Intent intent =IntentFactory.returnAppPanActivity(context).putExtra("imgType", "panCard");
        context.startActivity(intent);
    }

    private boolean isDOB(String str){
        int digits = 0;
        int slash = 0;
        boolean flag = false;
        if (str!=null){
            for (int m=0;m<str.length();m++){
                Character c = str.charAt(m);
                if (Character.isDigit(c)){
                    digits++;
                }
                if (c.equals('/')){
                    slash++;
                }
            }
            if (digits==8&&slash==2){
                flag =true;
            }

        }
        Log.d(TAG, "countDigits: word "+str +" digits "+digits);
        return flag;
    }

    private void detectSignature(Text text,Bitmap bitmap) {
        boolean isSignatureAvailable = false;
        Log.d(TAG, "detectSignature: complete_text "+text.getText());
        List<Text.TextBlock> blocks = text.getTextBlocks();

        int i=0;
        while (i<blocks.size()){

            if (isSignatureAvailable){
                Log.d(TAG, "detectSignature: ");
            }else{
                if (blocks.get(i).getText().contains("Sign") || blocks.get(i).getText().contains("/ Sign") || blocks.get(i).getText().contains("ature")){
                    Log.d(TAG, "detectSignature: "+blocks.get(i).getText());
                    isSignatureAvailable = true;
                    Rect bounds = blocks.get(i).getBoundingBox();
                    if (bounds!=null){
                        encodeDetectedSignature(bounds,bitmap);
                        saveDetectedSignature(bounds,bitmap);
                    }
                }
            }
            i++;
        }

        if (!isSignatureAvailable){
            Toast.makeText(context, "Signature not found", Toast.LENGTH_SHORT).show();
//            extractSignatureBitmap();
        }
    }

    private void encodeDetectedSignature(Rect bounds, Bitmap bitmap) {
        Log.d(TAG, "encodeDetectedSignature: boundsDetails ->  left :: "+(bounds.left) +" top :: "+(bounds.top-100) + "height :: "+bounds.height()+" width :: "+bounds.width());
        try {
//            Bitmap croppedBitmap = Bitmap.createBitmap(bitmap,bounds.left-50, bounds.top-250, bounds.width()+350, bounds.height()+150);
            Bitmap croppedBitmap = Bitmap.createBitmap(bitmap,bounds.left-50, bounds.top-250, bounds.width()+250, bounds.height()+150);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            signatureEncoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
            BitmapHolder.getInstance().setPanSignatureImageEncoded(signatureEncoded);
            Log.d(TAG, "encodeDetectedSignature: "+signatureEncoded);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Please put full image inside camera view ", Toast.LENGTH_SHORT).show();
        }
    }


    public void detectFace(Context mContext,Bitmap mBitmap){

        InputImage image = InputImage.fromBitmap(mBitmap,0);
        FaceDetectorOptions options =
                new FaceDetectorOptions.Builder()
                        .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
                        .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                        .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                        .setMinFaceSize(0.15f)
                        .enableTracking()
                        .build();

        FaceDetector detector = FaceDetection.getClient(options);
        detector.process(image).addOnSuccessListener(new OnSuccessListener<List<Face>>() {
            @Override
            public void onSuccess(List<Face> faces) {
                if (faces!=null){
                    processFaces(faces,mBitmap);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        }).addOnCompleteListener(new OnCompleteListener<List<Face>>() {
            @Override
            public void onComplete(@NonNull Task<List<Face>> task) {

            }
        });

    }

    private void dismissProgressDialog(){
        if (progressDialog!=null){
            progressDialog.dismiss();
        }
    }
    private void processFaces(List<Face> faces,Bitmap  bitmap) {
        if (faces.size()==1){
            Log.d(TAG, "processFaces: one face detected");
            Face currFace =faces.get(0);
            encodeDetectedFace(currFace,bitmap);
            saveDetectedFace(currFace,bitmap);
            dismissProgressDialog();
        }else  if (faces.size()>1){
            Log.d(TAG, "processFaces: multiple face detected");
            userImageEncoded = "multiple_faces";
            validateAndSendData();
            dismissProgressDialog();
        }else{
            Log.d(TAG, "processFaces: no face detected");
            userImageEncoded = "no_faces";
            validateAndSendData();
            dismissProgressDialog();
        }
    }

    private void saveDetectedFace(Face currFace,Bitmap bitmap) {
        Rect bounds = currFace.getBoundingBox();
        Bitmap croppedBitmap = Bitmap.createBitmap(bitmap,bounds.left-50, bounds.top-150, bounds.width()+100, bounds.height()+200);
        try {
            //Write file
            String filename = "detected_bitmap.png";
            FileOutputStream stream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            //Cleanup
            stream.close();
            croppedBitmap.recycle();
            isUserImageSaved = true;
            Log.d(TAG, "saveDetectedFace: detected face saved");

            progressDialog.dismiss();
            validateAndSendData();

//            //Pop intent
//            Intent in1 = new Intent(context, FullImageView.class);
//            in1.putExtra(Constants.DETECTED_FACE_FROM_DOCUMENT, filename);
//            context.startActivity(in1);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void saveDetectedSignature(Rect bounds, Bitmap bitmap) {
        Log.d(TAG, "saveDetectedSignature: boundsDetails ->  left :: "+(bounds.left) +" top :: "+(bounds.top-100) + "height :: "+bounds.height()+" width :: "+bounds.width());
        try {
            Bitmap croppedBitmap = Bitmap.createBitmap(bitmap,bounds.left-50, bounds.top-150, bounds.width()+100, bounds.height()+250);

            //Write file
            String filename = "detected_signature.png";
            FileOutputStream stream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            //Cleanup
            stream.close();
            croppedBitmap.recycle();
            isUserSignatureSaved = true;
            Log.d(TAG, "saveDetectedSignature: detected signature saved");

            CountDownTimer faceTimer = new CountDownTimer(1000,500) {
                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {
                    detectFace(context,bitmap);
                }
            };

            faceTimer.start();


            /*//Pop intent
            Intent in1 = new Intent(context, FullImageView.class);
            in1.putExtra(Constants.DETECTED_SIGNATURE_FROM_DOCUMENT, filename);
            context.startActivity(in1);*/
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Please put full image inside camera view ", Toast.LENGTH_SHORT).show();
        }
    }
    private void encodeDetectedFace(Face currFace, Bitmap bitmap) {
        Rect bounds = currFace.getBoundingBox();

        try {
//            Bitmap croppedBitmap = Bitmap.createBitmap(bitmap,bounds.left-50, bounds.top-250, bounds.width()+350, bounds.height()+150);
            Bitmap croppedBitmap = Bitmap.createBitmap(bitmap,bounds.left, bounds.top, bounds.width(), bounds.height());

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            userImageEncoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
            BitmapHolder.getInstance().setPanFaceImageEncoded(userImageEncoded);
            Log.d(TAG, "encodeDetectedFace: "+userImageEncoded);

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Please put full image inside camera view ", Toast.LENGTH_SHORT).show();
        }
    }

}