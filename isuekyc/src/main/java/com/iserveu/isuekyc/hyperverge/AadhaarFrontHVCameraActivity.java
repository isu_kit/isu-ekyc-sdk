package com.iserveu.isuekyc.hyperverge;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.face.Face;
import com.google.mlkit.vision.face.FaceDetection;
import com.google.mlkit.vision.face.FaceDetector;
import com.google.mlkit.vision.face.FaceDetectorOptions;
import com.iserveu.isuekyc.Constants;
import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.aadhaar_front.AadhaarFrontImageProcessor;
import com.iserveu.isuekyc.aadhaar_front.AadhaarFrontModelISUeKyc;
import com.iserveu.isuekyc.bitmap.BitmapHolder;
import com.iserveu.isuekyc.dialogs.DialogFactory;
import com.iserveu.isuekyc.face_detection.CapturedRecognition;
import com.iserveu.isuekyc.face_detection.CurrentRecognition;
import com.iserveu.isuekyc.intent.IntentFactory;

import org.tensorflow.lite.Interpreter;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AadhaarFrontHVCameraActivity extends AppCompatActivity {

    private NewOnBoardingViewModel onBoardingViewModel;
    private String frontAadhaarImageUrl = "";
    private String aadhaarNumber = "";
    private String aadhaarName = "";
    private float[][] embeedings;
    private HashMap<String, CapturedRecognition.Recognition> registered = new HashMap<>(); //saved Faces
    int OUTPUT_SIZE=192;
    private int inputSize = 112;
    private int[] intValues;
    private boolean isModelQuantized=false;
    private float IMAGE_MEAN = 128.0f;
    private float IMAGE_STD = 128.0f;
    private Interpreter tfLite;
    private float distance= 1.0f;
    private String modelFile="mobile_face_net.tflite"; //model name
    private Bitmap rotatedBitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aadhaar_front_hvcamera);
        onBoardingViewModel = new ViewModelProvider(this).get(NewOnBoardingViewModel.class);
        onBoardingViewModel.getAadhaarDocumentImageUrl(AadhaarFrontHVCameraActivity.this).observe(AadhaarFrontHVCameraActivity.this, hypervergeDocumentModel -> {
            if (hypervergeDocumentModel.getImageUrl() != null) {
                try {
                    frontAadhaarImageUrl = hypervergeDocumentModel.getImageUrl();
                    Constants.frontAadhaarUrl = frontAadhaarImageUrl;
                    DialogFactory.showImageCaptureInProgressDialog(this, "Wait...");
                    fetchFrontAadhaar();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(AadhaarFrontHVCameraActivity.this,
                        "Please upload your valid Aadhaar image",
                        Toast.LENGTH_SHORT).show();

            }
        });
    }

    public void fetchFrontAadhaar() {
        if (!frontAadhaarImageUrl.equals("")) {
            startOcrCall(frontAadhaarImageUrl);
        }
    }

    private void startOcrCall(String docImage) {

        onBoardingViewModel.fetchOcrDataFromCaptureDocuments(AadhaarFrontHVCameraActivity.this, docImage).observe(this, hypervergeDocumentModel -> {
            if (hypervergeDocumentModel.getStatus() != null
                    && hypervergeDocumentModel.getStatus().equalsIgnoreCase("success")) {
                try {
                    //new requirement changes - match aadhar card with aadhar number entered by the user.
                    if (hypervergeDocumentModel.getAadharNumber() != null
                            && !hypervergeDocumentModel.getAadharNumber().isEmpty()
                            && !hypervergeDocumentModel.getAadhaarName().isEmpty()) {
                        String getAadharNumber = hypervergeDocumentModel.getAadharNumber();
                        aadhaarName = hypervergeDocumentModel.getAadhaarName();
                        aadhaarNumber = getAadharNumber;

                        String upperName = aadhaarName.toUpperCase();
                        Constants.aadhaarFace = hypervergeDocumentModel.getFaceImage();
                        BitmapHolder.getInstance().setAadhaarFaceImageEncoded(Constants.aadhaarFace);
                        Bitmap userBitmap = decodeBitmap(Constants.aadhaarFace);
                        Matrix rotationMatrix = new Matrix();
                        if(userBitmap.getWidth() >= userBitmap.getHeight()){
                            rotationMatrix.setRotate(0);
                        }else{
                            rotationMatrix.setRotate(90);
                        }
                        rotatedBitmap = Bitmap.createBitmap(userBitmap,0,0,userBitmap.getWidth(),userBitmap.getHeight(),rotationMatrix,true);

//                        showData();
                        detectFace(this,rotatedBitmap);

                    }
//


                } catch (Exception e) {

                    e.printStackTrace();
                }
            } else {
                Toast.makeText(AadhaarFrontHVCameraActivity.this,
                        "Please upload your valid Aadhaar image",
                        Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void showData() {
        DialogFactory.dismissCameraProgressDialog();
        AadhaarFrontModelISUeKyc.getInstance().setAadhaarName(aadhaarName)
                .setAadhaarNumber(aadhaarNumber)
                .setAadhaarDOB("").setAadhaarVIDNumber("")
                .setAadhaarGender("").setAadhaarEncodedUserImage(Constants.aadhaarFace);

        AadhaarFrontHVCameraActivity.this.startActivity(IntentFactory.returnAppMainActivity(this)
                .putExtra("imgType", "aadharFront"));
    }



    public void detectFace(Context mContext, Bitmap mBitmap){

        InputImage image = InputImage.fromBitmap(mBitmap,0);
        FaceDetectorOptions options =
                new FaceDetectorOptions.Builder()
                        .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
                        .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
                        .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
                        .setMinFaceSize(0.15f)
                        .enableTracking()
                        .build();

        FaceDetector detector = FaceDetection.getClient(options);
        detector.process(image).addOnSuccessListener(new OnSuccessListener<List<Face>>() {
            @Override
            public void onSuccess(List<Face> faces) {
                if (faces!=null){
                    if (faces.size()>0){
                        processFaces(faces,mBitmap);
                    }else{
                        DialogFactory.dismissCameraProgressDialog();
                        finish();
                        Toast.makeText(AadhaarFrontHVCameraActivity.this, "No faces detected", Toast.LENGTH_SHORT).show();

                    }
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                DialogFactory.dismissCameraProgressDialog();
                finish();

            }
        }).addOnCompleteListener(new OnCompleteListener<List<Face>>() {
            @Override
            public void onComplete(@NonNull Task<List<Face>> task) {

            }
        });

    }


    private void processFaces(List<Face> faces,Bitmap  bitmap) {
        if (faces.size()==1){
            Log.d(TAG, "processFaces: One face detected");
            Face currFace =faces.get(0);
            saveFaceDetailsToTrainModel(currFace,bitmap);

        }else  if (faces.size()>1){
            showData();
        }else{
            Toast.makeText(AadhaarFrontHVCameraActivity.this, "No faces detected", Toast.LENGTH_SHORT).show();
            showData();
        }
    }

    private void saveFaceDetailsToTrainModel(Face currFace, Bitmap bitmap) {
//        registered = readFromSP();
        Rect bounds = currFace.getBoundingBox();
        RectF boundingBox = new RectF(currFace.getBoundingBox());
        Bitmap cropped_face = getCropBitmapByCPU(bitmap, boundingBox);
        Bitmap scaled = getResizedBitmap(cropped_face, 112, 112);
//        Bitmap actualFace = Bitmap.createBitmap(bitmap,bounds.left, bounds.top, bounds.width(), bounds.height());


        recognizeImage(scaled);



    }
    private static Bitmap getCropBitmapByCPU(Bitmap source, RectF cropRectF) {
        Bitmap resultBitmap = Bitmap.createBitmap((int) cropRectF.width(),
                (int) cropRectF.height(), Bitmap.Config.ARGB_8888);
        Canvas cavas = new Canvas(resultBitmap);

        // draw background
        Paint paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setColor(Color.WHITE);
        cavas.drawRect(
                new RectF(0, 0, cropRectF.width(), cropRectF.height()),
                paint);

        Matrix matrix = new Matrix();
        matrix.postTranslate(-cropRectF.left, -cropRectF.top);

        cavas.drawBitmap(source, matrix, paint);

        if (source != null && !source.isRecycled()) {
            source.recycle();
        }

        return resultBitmap;
    }

    public void recognizeImage(final Bitmap bitmap) {
        if (bitmap!=null){
            Log.d(TAG, "recognizeImage: called");
            //Create ByteBuffer to store normalized image

            ByteBuffer imgData = ByteBuffer.allocateDirect(1 * inputSize * inputSize * 3 * 4);

            imgData.order(ByteOrder.nativeOrder());

            intValues = new int[inputSize * inputSize];

            //get pixel values from Bitmap to normalize
            bitmap.getPixels(intValues, 0, bitmap.getWidth(), 0, 0, bitmap.getWidth(), bitmap.getHeight());

            imgData.rewind();

            for (int i = 0; i < inputSize; ++i) {
                for (int j = 0; j < inputSize; ++j) {
                    int pixelValue = intValues[i * inputSize + j];
                    if (isModelQuantized) {
                        // Quantized model
                        imgData.put((byte) ((pixelValue >> 16) & 0xFF));
                        imgData.put((byte) ((pixelValue >> 8) & 0xFF));
                        imgData.put((byte) (pixelValue & 0xFF));
                    } else { // Float model
                        imgData.putFloat((((pixelValue >> 16) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                        imgData.putFloat((((pixelValue >> 8) & 0xFF) - IMAGE_MEAN) / IMAGE_STD);
                        imgData.putFloat(((pixelValue & 0xFF) - IMAGE_MEAN) / IMAGE_STD);

                    }
                }
            }
            //imgData is input to our model
            Object[] inputArray = {imgData};

            Map<Integer, Object> outputMap = new HashMap<>();


            embeedings = new float[1][OUTPUT_SIZE]; //output of model will be stored in this variable
            Log.d(TAG, "recognizeImage: embeedings :: "+embeedings.toString());

            outputMap.put(0, embeedings);

            try{




                tfLite=new Interpreter(loadModelFile((Activity) this,modelFile));
                tfLite.runForMultipleInputsOutputs(inputArray, outputMap); //Run model

                /*CapturedRecognition.Recognition  recognition = new CapturedRecognition.Recognition("0", "", -1f);
                recognition.setExtra(embeedings);*/
                CapturedRecognition.Recognition.getInstance().initClassifier("0", "", -1f).setExtra(embeedings);
                registered.put(aadhaarName,CapturedRecognition.Recognition.getInstance());

                Object objCurrent = CurrentRecognition.Recognition.getInstance().getExtra();
                Object objCaptured = CapturedRecognition.Recognition.getInstance().getExtra();


                /*SimilarityClassifierHandler.Recognition.getInstance().initClassifier( "0", "", -1f).setCapturedExtra(embeedings);
                Object obj = SimilarityClassifierHandler.Recognition.getInstance().getCapturedExtra();
                Log.d(TAG, "recognizeImage: obj "+obj);
                registered.put(name,SimilarityClassifierHandler.Recognition.getInstance());
                Log.d(TAG, "recognizeImage: embeedings :: "+ Arrays.deepToString(embeedings));
                Log.d(TAG, "saveFaceDetailsToTrainModel: SimilarityClassifierHandler "+SimilarityClassifierHandler.Recognition.getInstance());
//                insertToSP(registered,0);
                Log.d(TAG, "saveFaceDetailsToTrainModel: registered "+registered.toString());*/

                showData();
            } catch (Exception e) {
                e.printStackTrace();
                Log.d(TAG, "recognizeImage: exception "+e.getLocalizedMessage());
            }
        }else{
            Log.d(TAG, "recognizeImage: bitmap is null");
        }

    }
    public Bitmap getResizedBitmap(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }
    private MappedByteBuffer loadModelFile(Activity activity, String MODEL_FILE) throws IOException {
        AssetFileDescriptor fileDescriptor = activity.getAssets().openFd(MODEL_FILE);
        FileInputStream inputStream = new FileInputStream(fileDescriptor.getFileDescriptor());
        FileChannel fileChannel = inputStream.getChannel();
        long startOffset = fileDescriptor.getStartOffset();
        long declaredLength = fileDescriptor.getDeclaredLength();
        return fileChannel.map(FileChannel.MapMode.READ_ONLY, startOffset, declaredLength);
    }

    private Bitmap decodeBitmap(String encodedImage) {

        byte[] decodedBytes = Base64.decode(encodedImage, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }
}