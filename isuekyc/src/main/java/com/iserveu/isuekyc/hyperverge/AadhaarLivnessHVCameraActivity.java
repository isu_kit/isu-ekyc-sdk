package com.iserveu.isuekyc.hyperverge;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.iserveu.isuekyc.Constants;
import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.aadhaar_front.AadhaarFrontModelISUeKyc;
import com.iserveu.isuekyc.face_detection.FaceMatchModelISUeKyc;
import com.iserveu.isuekyc.intent.IntentFactory;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URL;

public class AadhaarLivnessHVCameraActivity extends AppCompatActivity {
    private static final String TAG = "AadhaarLivenessHVCamera";
    private NewOnBoardingViewModel onBoardingViewModel;
    private int matchScore = 0;
    private String selfieImageUrl = "";
    private String frontAadhaarImageUrl = "";
    private String aadhaarNumber = "";
    private String aadhaarName = "";
    private String aadhaarFace = "";
    private String encoded ="";
     private Bitmap bmp = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        frontAadhaarImageUrl = Constants.frontAadhaarUrl;
         onBoardingViewModel =new ViewModelProvider(this).get(NewOnBoardingViewModel.class);
        onBoardingViewModel.getFaceLiveSelfi(AadhaarLivnessHVCameraActivity.this).observe(this, hypervergeDocumentModel -> {
            if (hypervergeDocumentModel.getImageUrl() != null) {
                try {

                    if (hypervergeDocumentModel.getIsSelfieLive().equals("yes")) {
                        selfieImageUrl = hypervergeDocumentModel.getImageUrl();
                        File filePath = new File(selfieImageUrl);
                        if(filePath.exists()){
                             bmp = BitmapFactory.decodeFile(filePath.getAbsolutePath());
                        }
                        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                        bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                        byte[] byteArray = byteArrayOutputStream .toByteArray();
                        encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
                        startDocumentVerificationProcess();
                    } else {
                        //show alert that picture is not live.
                        showAlert("Capture your valid Aadhaar card image");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                Toast.makeText(AadhaarLivnessHVCameraActivity.this,
                        "Please upload your valid Aadhaar image",
                        Toast.LENGTH_SHORT).show();


            }
        });

    }

    private void startDocumentVerificationProcess(){

        onBoardingViewModel.verifyDocument(AadhaarLivnessHVCameraActivity.this, frontAadhaarImageUrl, selfieImageUrl).observe(this, hypervergeDocumentModel -> {
            if (hypervergeDocumentModel.getStatus() != null
                    && hypervergeDocumentModel.getStatus().equalsIgnoreCase("success")) {
                try {
                    //start uploading data to server
                    matchScore = hypervergeDocumentModel.getMatchScore();
                    if(matchScore <=60){
                        showAlert("The selfie you uploaded does not match with the photo on your Aadhar card. Please take your selfie again");

                    }else {

                        showData();
                    }
                } catch (Exception e) {

                    showAlert("The selfie you uploaded does not match with the photo on your Aadhar card. Please take your selfie again");
                    e.printStackTrace();
                    Log.e(TAG, "getting exp: "+e.getLocalizedMessage());
                }
            } else {


                Toast.makeText(AadhaarLivnessHVCameraActivity.this,
                        "Please upload your valid Aadhaar image",
                        Toast.LENGTH_SHORT).show();


            }
        });
    }

    private void showAlert(String message) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(AadhaarLivnessHVCameraActivity.this);
        alertBuilder.setMessage(message);
        alertBuilder.setCancelable(true);
        alertBuilder.setPositiveButton(
                "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alertDialog = alertBuilder.create();
        alertDialog.show();
    }
    private void showData() {
        FaceMatchModelISUeKyc.getInstance().setFaceImageEncoded(encoded);
        AadhaarFrontModelISUeKyc.getInstance().setAadhaarEncodedUserImage(Constants.aadhaarFace);

        this.startActivity(IntentFactory.returnAppMainActivity(this)
                .putExtra("imgType","faceMatch_with_aadhaar"));
    }
}