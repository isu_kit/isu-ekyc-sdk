package com.iserveu.isuekyc.hyperverge;

public class HyperVergeDocumentModel {

    private String errorMessage;
    private String statusMessage;
    private String imageUrl;
    private String ImageFullPath;
    private String outputImageUrl;
    private String faceImage;

    public String getFaceImage() {
        return faceImage;
    }

    public void setFaceImage(String faceImage) {
        this.faceImage = faceImage;
    }




    public String getOutputImageUrl() {
        return outputImageUrl;
    }

    public void setOutputImageUrl(String outputImageUrl) {
        this.outputImageUrl = outputImageUrl;
    }


    public String getIsSelfieLive() {
        return isSelfieLive;
    }

    public void setIsSelfieLive(String isSelfieLive) {
        this.isSelfieLive = isSelfieLive;
    }

    public String getSelfiLiveNessScore() {
        return selfiLiveNessScore;
    }

    public void setSelfiLiveNessScore(String selfiLiveNessScore) {
        this.selfiLiveNessScore = selfiLiveNessScore;
    }

    public String getToBeReviewed() {
        return toBeReviewed;
    }

    public void setToBeReviewed(String toBeReviewed) {
        this.toBeReviewed = toBeReviewed;
    }

    private String isSelfieLive;
    private String selfiLiveNessScore;
    private String toBeReviewed;

    public String getPanNumber() {
        return panNumber;
    }

    public void setPanNumber(String panNumber) {
        this.panNumber = panNumber;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        this.aadharNumber = aadharNumber;
    }

    private String panNumber;
    private String panName;
    private String aadharNumber;
    private String aadhaarName;
    private String backAadhaarNumber;
    private String aadhaarAddress;
    private String aadhaarPin;
    private String aadhaarCity;
    private String aadhaarState;

    public String getAadhaarCity() {
        return aadhaarCity;
    }

    public void setAadhaarCity(String aadhaarCity) {
        this.aadhaarCity = aadhaarCity;
    }

    public String getAadhaarState() {
        return aadhaarState;
    }

    public void setAadhaarState(String aadhaarState) {
        this.aadhaarState = aadhaarState;
    }

    public String getBackAadhaarNumber() {
        return backAadhaarNumber;
    }

    public void setBackAadhaarNumber(String backAadhaarNumber) {
        this.backAadhaarNumber = backAadhaarNumber;
    }

    public String getAadhaarAddress() {
        return aadhaarAddress;
    }

    public void setAadhaarAddress(String aadhaarAddress) {
        this.aadhaarAddress = aadhaarAddress;
    }

    public String getAadhaarPin() {
        return aadhaarPin;
    }

    public void setAadhaarPin(String aadhaarPin) {
        this.aadhaarPin = aadhaarPin;
    }

    public String getPanName() {
        return panName;
    }

    public void setPanName(String panName) {
        this.panName = panName;
    }

    public String getAadhaarName() {
        return aadhaarName;
    }

    public void setAadhaarName(String aadhaarName) {
        this.aadhaarName = aadhaarName;
    }

    public int getMatchScore() {
        return matchScore;
    }

    public void setMatchScore(int matchScore) {
        this.matchScore = matchScore;
    }

    private int matchScore;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    private String errorCode;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageFullPath() {
        return ImageFullPath;
    }

    public void setImageFullPath(String imageFullPath) {
        ImageFullPath = imageFullPath;
    }
}
