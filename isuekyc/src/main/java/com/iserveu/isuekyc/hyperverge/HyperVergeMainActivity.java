package com.iserveu.isuekyc.hyperverge;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.iserveu.isuekyc.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import co.hyperverge.hypersnapsdk.activities.HVDocsActivity;
import co.hyperverge.hypersnapsdk.activities.HVFaceActivity;
import co.hyperverge.hypersnapsdk.listeners.APICompletionCallback;
import co.hyperverge.hypersnapsdk.listeners.DocCaptureCompletionHandler;
import co.hyperverge.hypersnapsdk.listeners.FaceCaptureCompletionHandler;
import co.hyperverge.hypersnapsdk.network.HVNetworkHelper;
import co.hyperverge.hypersnapsdk.objects.HVDocConfig;
import co.hyperverge.hypersnapsdk.objects.HVError;
import co.hyperverge.hypersnapsdk.objects.HVFaceConfig;
import co.hyperverge.hypersnapsdk.objects.HVResponse;

public class HyperVergeMainActivity extends AppCompatActivity implements View.OnClickListener , RadioGroup.OnCheckedChangeListener {

    private HVDocConfig.Document selectedDocument;
    HVFaceConfig.LivenessMode mode = HVFaceConfig.LivenessMode.TEXTURELIVENESS;
    TextView resultView;
    private String faceImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hyper_verge_main);
        resultView = (TextView) findViewById(R.id.tv_result);
        findViewById(R.id.tv_a4).setOnClickListener(this);
        findViewById(R.id.tv_other).setOnClickListener(this);
        findViewById(R.id.tv_card).setOnClickListener(this);
        findViewById(R.id.tv_passport).setOnClickListener(this);
        findViewById(R.id.tv_face).setOnClickListener(this);
        ((RadioGroup) (findViewById(R.id.face_value))).setOnCheckedChangeListener(this);

    }

    public void startAppropriateDocumentActivity(final HVDocConfig docConfig) {

        HVDocsActivity.start(HyperVergeMainActivity.this, docConfig, new DocCaptureCompletionHandler() {
            @Override
            public void onResult(HVError hvError, HVResponse hvResponse) {
                if (hvError != null) {
                    resultView.setText("ERROR: " + hvError.getErrorCode() + " Msg: " + hvError.getErrorMessage());
                } else {
                    resultView.setText("RESULT: " + hvResponse.toString());
                    try {
                        startOcrCall(hvResponse.getImageURI());
                        Glide.with(HyperVergeMainActivity.this).load(hvResponse.getImageURI()).into((ImageView) findViewById(R.id.iv_result));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                ScrollView sv = findViewById(R.id.sv_main);
                sv.fullScroll(View.FOCUS_DOWN);
            }
        });
    }

    private void startOcrCall(String docImage) {
        //   String imageUri = resultFromDocCapture.getString("imageUri"); //This is the result from document capture

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("outputImageUrl","yes");
        }catch (JSONException e){
            e.printStackTrace();
        }

        String endpoint = "https://ind-docs.hyperverge.co/v2.0/readKYC";
        APICompletionCallback completionCallback = new APICompletionCallback() {
            @Override
            public void onResult(HVError hvError, HVResponse hvResponse) {
                if (hvError != null) {
                    resultView.setText("ERROR: " + hvError.getErrorCode() + " Msg: " + hvError.getErrorMessage());
                } else {
                    /*Start validation*/
                    startValidation(docImage);
                }
            }
        };
        HVNetworkHelper.makeOCRCall(HyperVergeMainActivity.this,endpoint,docImage,jsonObject,null,completionCallback);
    }

    private void startValidation(String docImage) {
//        String docImageUri = resultFromDocCapture.getString("imageUri"); //This is the result from document capture
//        String faceImageUri = resultFromFaceCapture.getString("imageUri"); //This is the result from face capture
        String endpoint = "https://ind-faceid.hyperverge.co/v1/photo/verifyPair";
        APICompletionCallback completionCallback = new APICompletionCallback() {
            @Override
            public void onResult(HVError hvError, HVResponse hvResponse) {
                if(hvError != null){
                    resultView.setText("ERROR: " + hvError.getErrorCode() + " Msg: " + hvError.getErrorMessage());
                }else{
                    resultView.setText("ERROR: " + hvError.getErrorCode() + " Msg: " + hvResponse.getAction());
                }
            }
        };
        HVNetworkHelper.makeFaceMatchCall(HyperVergeMainActivity.this, endpoint, faceImageUri, docImage, null, null, completionCallback);
    }


    public void setLocale() {
        /**
         * To set a locale for country, the contry code is set in Locale constructor
         * Locale locale = new Locale("vi") - Vietnam
         */
        Locale locale = new Locale("IN");
        Locale.setDefault(locale);
        Configuration config = getBaseContext().getResources().getConfiguration();
        config.locale = locale;
        Resources res = getBaseContext().getResources();
        res.updateConfiguration(config, res.getDisplayMetrics());

    }

    public void startFaceCaptureActivity() {
        /**
         * HVFaceConfig is the configuration class to set parameters for HVFaceActivity
         */
        HVFaceConfig config = new HVFaceConfig();
        // config.setLivenessMode(mode);
        config.setFaceCaptureTitle(" Face capture  ");
        config.setShouldShowInstructionPage(true);
        config.setShouldReturnFullImageUrl(true);
        config.setShouldUseEnhancedCameraFeatures(true);
        HVFaceActivity.start(HyperVergeMainActivity.this, config, new FaceCaptureCompletionHandler() {
            @Override
            public void onResult(HVError error, HVResponse result) {
                if (error != null) {
                    resultView.setText(" Msg: " + error.getErrorMessage() + "ERROR: " + error.getErrorCode());
                } else {
                    try {
                        resultView.setText(result.toString());
                        Toast.makeText(HyperVergeMainActivity.this, result.getImageURI(), Toast.LENGTH_SHORT).show();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    faceImageUri= result.getImageURI();
                                    Glide.with(HyperVergeMainActivity.this).load(result.getImageURI()).dontAnimate().into((ImageView) findViewById(R.id.iv_result));
                                    startCardRead();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, 100);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    /**
     * start card read activity
     *
     */

    private void startCardRead(){
        HVDocConfig docConfig = new HVDocConfig();
        docConfig.setShouldShowFlashIcon(false);
        docConfig.setDocReviewDescription("Is your document fully visible, glare free and not blurred ?");
        docConfig.setDocReviewTitle("Review your photo");
        docConfig.setDocCaptureTitle("Docs Capture");
        docConfig.setDocCaptureDescription("Make sure your document is without any glare and is fully inside");
        docConfig.setDocCaptureSubText("Front side");
        docConfig.setShouldShowReviewScreen(true);
        docConfig.setShouldShowInstructionPage(true);
        selectedDocument = HVDocConfig.Document.CARD;
        docConfig.setDocumentType(selectedDocument);
        startAppropriateDocumentActivity(docConfig);
    }
    @Override
    public void onClick(View view) {
        HVDocConfig docConfig = new HVDocConfig();
        docConfig.setShouldShowFlashIcon(false);
        docConfig.setDocReviewDescription("Is your document fully visible, glare free and not blurred ?");
        docConfig.setDocReviewTitle("Review your photo");
        docConfig.setDocCaptureTitle("Docs Capture");
        docConfig.setDocCaptureDescription("Make sure your document is without any glare and is fully inside");
        docConfig.setDocCaptureSubText("Front side");
        docConfig.setShouldShowReviewScreen(true);
        docConfig.setShouldShowInstructionPage(true);
        if (view.getId() == R.id.tv_a4) {
            selectedDocument = HVDocConfig.Document.A4;

            docConfig.setDocumentType(selectedDocument);
            startAppropriateDocumentActivity(docConfig);
        }
        if (view.getId() == R.id.tv_card) {
            selectedDocument = HVDocConfig.Document.CARD;
            docConfig.setDocumentType(selectedDocument);
            startAppropriateDocumentActivity(docConfig);
        }
        if (view.getId() == R.id.tv_other) {
            selectedDocument = HVDocConfig.Document.OTHER;
            docConfig.setDocumentType(selectedDocument);
            startAppropriateDocumentActivity(docConfig);
        }
        if (view.getId() == R.id.tv_passport) {
            selectedDocument = HVDocConfig.Document.PASSPORT;
            docConfig.setDocumentType(selectedDocument);
            startAppropriateDocumentActivity(docConfig);
        }

        if (view.getId() == R.id.tv_face) {
            startFaceCaptureActivity();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int id) {
        if (id == R.id.texture_liveness) {
            mode = HVFaceConfig.LivenessMode.TEXTURELIVENESS;
        } else if (id == R.id.none_liveness) {
            mode = HVFaceConfig.LivenessMode.NONE;
        }
    }
}