package com.iserveu.isuekyc.hyperverge;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import co.hyperverge.hypersnapsdk.activities.HVDocsActivity;
import co.hyperverge.hypersnapsdk.activities.HVFaceActivity;
import co.hyperverge.hypersnapsdk.listeners.APICompletionCallback;
import co.hyperverge.hypersnapsdk.listeners.DocCaptureCompletionHandler;
import co.hyperverge.hypersnapsdk.listeners.FaceCaptureCompletionHandler;
import co.hyperverge.hypersnapsdk.network.HVNetworkHelper;
import co.hyperverge.hypersnapsdk.objects.HVDocConfig;
import co.hyperverge.hypersnapsdk.objects.HVError;
import co.hyperverge.hypersnapsdk.objects.HVFaceConfig;
import co.hyperverge.hypersnapsdk.objects.HVResponse;

public class NewOnBoardingRepository {
    /**
     * service class will be instantiate here , best we need to do is adding Hit
     * add dependency injection module.
     */
    private static final String TAG = NewOnBoardingRepository.class.getSimpleName();
    /**
     * Document and Liveness have been moved to their respective parent configuration classes.
     */
    private HVDocConfig.Document selectedDocument;
    HVFaceConfig.LivenessMode mode = HVFaceConfig.LivenessMode.TEXTURELIVENESS;
    private HVDocConfig docConfig;
    private HVFaceConfig config;

    /**
     * request for aadhar document
     * using hyper-verge library
     */
    public LiveData<HyperVergeDocumentModel> getAadhaarDocument(Context ctx) {
        initDocType();
        final MutableLiveData<HyperVergeDocumentModel> data = new MutableLiveData<>();
        HyperVergeDocumentModel hyperVergeDocumentModel = new HyperVergeDocumentModel();
        HVDocsActivity.start(ctx, docConfig, new DocCaptureCompletionHandler() {
            @Override
            public void onResult(HVError hvError, HVResponse hvResponse) {
                if (hvError != null) {
                    hyperVergeDocumentModel.setErrorMessage(hvError.getErrorMessage());
                    hyperVergeDocumentModel.setErrorCode(String.valueOf(hvError.getErrorCode()));
                    data.setValue(hyperVergeDocumentModel);
                } else {
                    hyperVergeDocumentModel.setImageFullPath(hvResponse.getFullImageURI());
                    hyperVergeDocumentModel.setImageUrl(hvResponse.getImageURI());
                    data.setValue(hyperVergeDocumentModel);
                }
            }
        });
        return data;
    }

    /**
     * request for selfi
     * using hyper-verge library
     */
    public LiveData<HyperVergeDocumentModel> getFaceLiveSelfi(Context ctx) {
        initFaceType();
        final MutableLiveData<HyperVergeDocumentModel> data = new MutableLiveData<>();
        HyperVergeDocumentModel hyperVergeDocumentModel = new HyperVergeDocumentModel();
        HVFaceActivity.start(ctx, config, new FaceCaptureCompletionHandler() {
            @Override
            public void onResult(HVError hvError, HVResponse hvResponse) {
                if (hvError != null) {
                    hyperVergeDocumentModel.setErrorMessage(hvError.getErrorMessage());
                    hyperVergeDocumentModel.setErrorCode(String.valueOf(hvError.getErrorCode()));
                    data.setValue(hyperVergeDocumentModel);
                } else {
                    //check liveness status
                    try {
                        JSONObject liveNessJsonObject = hvResponse.getApiResult();
                        if (liveNessJsonObject != null) {
                            if (liveNessJsonObject.has("result")) {
                                JSONObject resultObject = liveNessJsonObject.getJSONObject("result");
                                if (resultObject != null) {
                                    if (resultObject.has("live")) {
                                        hyperVergeDocumentModel.setIsSelfieLive(resultObject.getString("live"));
                                    }
                                    if (resultObject.has("liveness-score")) {
                                        hyperVergeDocumentModel.setSelfiLiveNessScore(resultObject.getString("liveness-score"));
                                    }
                                    if (resultObject.has("to-be-reviewed")) {
                                        hyperVergeDocumentModel.setToBeReviewed(resultObject.getString("to-be-reviewed"));
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                    }
                    hyperVergeDocumentModel.setImageFullPath(hvResponse.getFullImageURI());
                    hyperVergeDocumentModel.setImageUrl(hvResponse.getImageURI());
                    data.setValue(hyperVergeDocumentModel);
                }
            }
        });
        return data;
    }

    /**
     * request for selfi
     * using hyper-verge library
     */
    public LiveData<HyperVergeDocumentModel> fetchOcrDataFromCaptureDocuments(Context ctx, String docImage) {
        final MutableLiveData<HyperVergeDocumentModel> data = new MutableLiveData<>();
        HyperVergeDocumentModel hyperVergeDocumentModel = new HyperVergeDocumentModel();
        hyperVergeDocumentModel.setOutputImageUrl("yes");
        String endpoint = "https://ind-docs.hyperverge.co/v2.0/readAadhaar";
        APICompletionCallback completionCallback = new APICompletionCallback() {
            @Override
            public void onResult(HVError hvError, HVResponse hvResponse) {
                try {
                    if (hvError != null) {
                        JSONObject dataObject = hvResponse.getApiResult();
                        if (dataObject != null) {
                            if (dataObject.has("status")) {
                                hyperVergeDocumentModel.setStatus(dataObject.getString("status"));
                            }
                            hyperVergeDocumentModel.setErrorMessage(hvError.getErrorMessage());
                            hyperVergeDocumentModel.setErrorCode(String.valueOf(hvError.getErrorCode()));
                            data.setValue(hyperVergeDocumentModel);
                        } else {
                            hyperVergeDocumentModel.setErrorMessage(hvError.getErrorMessage());
                            hyperVergeDocumentModel.setErrorCode(String.valueOf(hvError.getErrorCode()));
                            data.setValue(hyperVergeDocumentModel);
                        }
                    } else {
                        JSONObject dataObject = hvResponse.getApiResult();
                        if (dataObject != null) {
                            if (dataObject.has("status")) {
                                hyperVergeDocumentModel.setStatus(dataObject.getString("status"));
                            }
                            /**
                             * get pan and aadhar number
                             */
                            if (dataObject.has("result")) {
                                JSONArray resultArray = dataObject.getJSONArray("result");
                                Log.e(TAG, "onResult: pan result " + resultArray);
                                if (resultArray != null && resultArray.length() > 0) {
                                    JSONObject resultObject = resultArray.getJSONObject(0);
                                    if (resultObject != null && resultObject.has("type")) {
                                        String type = resultObject.getString("type");

                                        Log.e(TAG, "onResult: type " + type);

                                        if (type != null && type.contains("pan")) {
                                            if (resultObject.has("details")) {
                                                JSONObject detailsObject = resultObject.getJSONObject("details");
                                                if (detailsObject != null && detailsObject.has("pan_no")) {
                                                    JSONObject panInfoObject = detailsObject.getJSONObject("pan_no");
                                                    if (panInfoObject != null && panInfoObject.has("value")) {
                                                        hyperVergeDocumentModel.setPanNumber(panInfoObject.getString("value"));
                                                    }
                                                }
                                                if (detailsObject != null && detailsObject.has("name")) {
                                                    JSONObject panInfoObject = detailsObject.getJSONObject("name");
                                                    if (panInfoObject != null && panInfoObject.has("value")) {
                                                        hyperVergeDocumentModel.setPanName(panInfoObject.getString("value"));
                                                    }
                                                }
                                            }
                                        } else if (type != null && type.contains("aadhaar_front_bottom")) {
                                            if (resultObject != null && resultObject.has("details")) {
                                                JSONObject detailsObject = resultObject.getJSONObject("details");
                                                if (detailsObject != null && detailsObject.has("aadhaar")) {
                                                    JSONObject aadhaarInfoObject = detailsObject.getJSONObject("aadhaar");
                                                    if (aadhaarInfoObject != null && aadhaarInfoObject.has("value")) {
                                                        hyperVergeDocumentModel.setAadharNumber(aadhaarInfoObject.getString("value"));
                                                    }
                                                }
                                                if (detailsObject != null && detailsObject.has("name")) {
                                                    JSONObject aadhaarInfoObject = detailsObject.getJSONObject("name");
                                                    if (aadhaarInfoObject != null && aadhaarInfoObject.has("value")) {
                                                        hyperVergeDocumentModel.setAadhaarName(aadhaarInfoObject.getString("value"));
                                                    }
                                                }
                                                if(detailsObject !=null && detailsObject.has("face")){
                                                    JSONObject aadhaarFace = detailsObject.getJSONObject("face");
                                                    if(aadhaarFace !=null && aadhaarFace.has("faceString")){
                                                        hyperVergeDocumentModel.setFaceImage(aadhaarFace.getString("faceString"));
                                                    }
                                                }
                                            }
                                        } else if (type != null && type.contains("aadhaar_back")) {
                                            if (resultObject != null && resultObject.has("details")) {
                                                JSONObject detailsObject = resultObject.getJSONObject("details");
                                                if (detailsObject != null && detailsObject.has("aadhaar")) {
                                                    JSONObject aadhaarInfoObject = detailsObject.getJSONObject("aadhaar");
                                                    if (aadhaarInfoObject != null && aadhaarInfoObject.has("value")) {
                                                        hyperVergeDocumentModel.setBackAadhaarNumber(aadhaarInfoObject.getString("value"));
                                                    }
                                                }
                                                if (detailsObject != null && detailsObject.has("address")) {
                                                    JSONObject aadhaarInfoObject = detailsObject.getJSONObject("address");
                                                    if (aadhaarInfoObject != null) {
                                                        if (aadhaarInfoObject.has("pin")) {
                                                            hyperVergeDocumentModel.setAadhaarPin(aadhaarInfoObject.getString("pin"));
                                                        }
                                                        String address = "";
                                                        if (aadhaarInfoObject.has("line1")) {
                                                            address = aadhaarInfoObject.getString("line1");
                                                        }
                                                        if (aadhaarInfoObject.has("line2")) {
                                                            address = address + ", " + aadhaarInfoObject.getString("line2");
                                                        }
                                                        hyperVergeDocumentModel.setAadhaarAddress(address);

                                                        if (aadhaarInfoObject.has("city")) {
                                                            hyperVergeDocumentModel.setAadhaarCity(aadhaarInfoObject.getString("city"));
                                                        }
                                                        if (aadhaarInfoObject.has("state")) {
                                                            hyperVergeDocumentModel.setAadhaarState(aadhaarInfoObject.getString("state"));
                                                        }

                                                    }
                                                }
                                            }
                                        }

                                    }
                                }
                            }
                            data.setValue(hyperVergeDocumentModel);
                        } else {
                            hyperVergeDocumentModel.setErrorMessage("Invalid document");
                            data.setValue(hyperVergeDocumentModel);
                        }
                    }
                } catch (Exception e) {
                    hyperVergeDocumentModel.setErrorMessage("Invalid document");
                    data.setValue(hyperVergeDocumentModel);
                }
            }
        };
        JSONObject docParams = new JSONObject();
        //JSONObject docHeaders = new JSONObject();
        try {
            //docHeaders.put("transactionId", "<Unique Customer Id>"); // For billing
            docParams.put("allowOnlyLiveDocument", "yes"); // Document Liveness Check
            docParams.put("faceCheck", "yes");

        } catch (JSONException e) {
        }
        HVNetworkHelper.makeOCRCall(ctx, endpoint, docImage, docParams, null, completionCallback);
        return data;
    }

    /**
     * request for selfi
     * using hyper-verge library
     */
    public LiveData<HyperVergeDocumentModel> verifyDocument(Context ctx, String docImage, String faceUri) {
        final MutableLiveData<HyperVergeDocumentModel> data = new MutableLiveData<>();
        HyperVergeDocumentModel hyperVergeDocumentModel = new HyperVergeDocumentModel();
        String endpoint = "https://ind-faceid.hyperverge.co/v1/photo/verifyPair";
        APICompletionCallback completionCallback = new APICompletionCallback() {
            @Override
            public void onResult(HVError hvError, HVResponse hvResponse) {
                if (hvError != null) {
                    hyperVergeDocumentModel.setErrorMessage(hvError.getErrorMessage());
                    hyperVergeDocumentModel.setErrorCode(String.valueOf(hvError.getErrorCode()));
                    data.setValue(hyperVergeDocumentModel);
                } else {
                    try {
                        JSONObject dataObject = hvResponse.getApiResult();
                        if (dataObject != null) {
                            String status = dataObject.getString("status");
                            if (status != null) {
                                hyperVergeDocumentModel.setStatus(status);
                            }
                            if (dataObject.has("result")) {
                                JSONObject resultJson = dataObject.getJSONObject("result");
                                if (resultJson != null && resultJson.has("match-score")) {
                                    int matchScore = resultJson.getInt("match-score");
                                    hyperVergeDocumentModel.setMatchScore(matchScore);
                                }
                            }
                        }
                        data.setValue(hyperVergeDocumentModel);
                    } catch (JSONException e) {
                        hyperVergeDocumentModel.setErrorMessage("Error in validation");
                        data.setValue(hyperVergeDocumentModel);
                    }
                }
            }
        };
        HVNetworkHelper.makeFaceMatchCall(ctx, endpoint, faceUri, docImage, null, null, completionCallback);
        return data;
    }

    /**
     * set doc config
     */
    private void initDocType() {
        docConfig = new HVDocConfig();
        docConfig.setShouldShowFlashIcon(false);
        docConfig.setDocReviewDescription("Is your document fully visible, glare free and not blurred ?");
        docConfig.setDocReviewTitle("Review your photo");
        docConfig.setDocCaptureTitle("Docs Capture");
        docConfig.setDocCaptureDescription("Make sure your document is without any glare and is fully inside");
        docConfig.setDocCaptureSubText("Front side");
        docConfig.setShouldShowReviewScreen(true);
        docConfig.setShouldShowInstructionPage(true);
        selectedDocument = HVDocConfig.Document.CARD;
        docConfig.setDocumentType(selectedDocument);
    }

    /**
     * set face selfi config
     */
    private void initFaceType() {
        /**
         * HVFaceConfig is the configuration class to set parameters for HVFaceActivity
         */
        config = new HVFaceConfig();
        // config.setLivenessMode(mode);
        config.setFaceCaptureTitle(" Face capture  ");
        config.setShouldShowInstructionPage(true);
        config.setShouldReturnFullImageUrl(true);
        config.setShouldUseEnhancedCameraFeatures(true);
    }
}
