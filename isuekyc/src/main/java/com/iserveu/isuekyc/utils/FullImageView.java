package com.iserveu.isuekyc.utils;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import com.iserveu.isuekyc.R;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class FullImageView extends AppCompatActivity {
    private ImageView imgFullImage, imgCancelBtn,imgDoneBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image_view);
        initViews();
        onClickListeners();
        if (getIntent().hasExtra(Constants.DETECTED_FACE_FROM_DOCUMENT)){
            Bitmap bmp = null;
            String filename = getIntent().getStringExtra(Constants.DETECTED_FACE_FROM_DOCUMENT);
            try {
                FileInputStream is = this.openFileInput(filename);
                bmp = BitmapFactory.decodeStream(is);
                showDetectedFaceFromDocument(bmp);
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else  if (getIntent().hasExtra(Constants.DETECTED_SIGNATURE_FROM_DOCUMENT)){
            Bitmap bmp = null;
            String filename = getIntent().getStringExtra(Constants.DETECTED_SIGNATURE_FROM_DOCUMENT);
            try {
                FileInputStream is = this.openFileInput(filename);
                bmp = BitmapFactory.decodeStream(is);
                showDetectedSignatureFromDocument(bmp);
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else{
            showFullImage();
        }

    }

    private void showDetectedFaceFromDocument(Bitmap bmp) {
        imgFullImage.setImageBitmap(bmp);
    }
    private void showDetectedSignatureFromDocument(Bitmap bmp) {
        imgFullImage.setImageBitmap(bmp);
    }

    private void onClickListeners() {
        imgCancelBtn.setOnClickListener(v -> {
            finish();
        });

        imgDoneBtn.setOnClickListener(view -> {
            Toast.makeText(this, "Image Captured", Toast.LENGTH_SHORT).show();
        });
    }

    private void initViews() {
        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }
        imgFullImage = findViewById(R.id.imgFullImage);
        imgCancelBtn = findViewById(R.id.imgCancelBtn);
        imgDoneBtn = findViewById(R.id.imgDoneBtn);


    }

    private void showFullImage() {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(openFileInput("myImage"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        imgFullImage.setImageBitmap(bitmap);
    }
}