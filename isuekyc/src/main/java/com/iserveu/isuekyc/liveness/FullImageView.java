package com.iserveu.isuekyc.liveness;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;

import com.iserveu.isuekyc.Constants;
import com.iserveu.isuekyc.R;
import com.iserveu.isuekyc.bitmap.BitmapHolder;
import com.iserveu.isuekyc.intent.IntentFactory;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

public class FullImageView extends AppCompatActivity {
    private static final String TAG = FullImageView.class.getSimpleName();
    private ImageView imgFullImage, imgCancelBtn,imgDoneBtn;
    /*// Face Mask
    private static final int TF_OD_API_INPUT_SIZE = 224;
    private static final boolean TF_OD_API_IS_QUANTIZED = false;
    private static final String TF_OD_API_MODEL_FILE = "mask_detector.tflite";
    private static final String TF_OD_API_LABELS_FILE = "file:///android_asset/mask_labelmap.txt";
    private Classifier detector;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image_view);
        initViews();
        onClickListeners();
        if (getIntent().hasExtra("liveness_image_uri")){
            Uri imageUri = Uri.parse(getIntent().getExtras().getString("liveness_image_uri"));
            Constants.selfiImageUri = imageUri.toString();
            showCapturedImage(imageUri);
        }
        if (getIntent().hasExtra("img_type")){
            showFullImage();
        }


        /*if (getIntent().hasExtra(Constants.DETECTED_FACE_FROM_DOCUMENT)){
            Bitmap bmp = null;
            String filename = getIntent().getStringExtra(Constants.DETECTED_FACE_FROM_DOCUMENT);
            try {
                FileInputStream is = this.openFileInput(filename);
                bmp = BitmapFactory.decodeStream(is);
                showDetectedFaceFromDocument(bmp);
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else  if (getIntent().hasExtra(Constants.DETECTED_SIGNATURE_FROM_DOCUMENT)){
            Bitmap bmp = null;
            String filename = getIntent().getStringExtra(Constants.DETECTED_SIGNATURE_FROM_DOCUMENT);
            try {
                FileInputStream is = this.openFileInput(filename);
                bmp = BitmapFactory.decodeStream(is);
                showDetectedSignatureFromDocument(bmp);
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else{
            showFullImage();
        }*/

    }

    private void showDetectedFaceFromDocument(Bitmap bmp) {

        imgFullImage.setImageBitmap(bmp);
    }
    private void showDetectedSignatureFromDocument(Bitmap bmp) {
        imgFullImage.setImageBitmap(bmp);
    }

    private void onClickListeners() {
        imgCancelBtn.setOnClickListener(v -> {
            finish();
        });

        imgDoneBtn.setOnClickListener(view -> {
            try {
                Bitmap bitmap = null;

                    bitmap = BitmapFactory.decodeStream(openFileInput("myImage"));
//                    FaceMaskDetectorProcessorNew.getInstance().detectFace(this,bitmap);
//                    MaskDetector.getInstance().initClassifier(this).detectFaceMask(bitmap);
//                PoseDetectionProcessor.getInstance().detectPose(this,bitmap);
//                ObjectDetectionProcessor.getInstance().detectObject(this,bitmap);
//                FaceDetectionProcessor.getInstance().detectFace(this,bitmap);
                startActivity(IntentFactory.returnAppMainActivity(this).putExtra("imgType","liveness_image"));

            }catch (Exception exception){
                Log.d(TAG, "onClickListeners: "+ exception.getLocalizedMessage());
            }
        });
    }

    private void initViews() {
        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }
        imgFullImage = findViewById(R.id.imgFullImage);
        imgCancelBtn = findViewById(R.id.imgCancelBtn);
        imgDoneBtn = findViewById(R.id.imgDoneBtn);


    }

    private void showFullImage() {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(openFileInput("myImage"));
            BitmapHolder.getInstance().setLiveNessCheckBitmap(bitmap);
//            initializeClassifier();
            encodeUserImage(bitmap);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        imgFullImage.setImageBitmap(bitmap);
    }

    private void showCapturedImage(Uri imageUri) {
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
//            mSelectedImage = AadhaarFrontCameraActivity.capturedBitmap;
            Matrix rotationMatrix = new Matrix();
           Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),rotationMatrix,true);
            imgFullImage.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String encodeUserImage(Bitmap bmp) {

        try {

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            String encoded = Base64.encodeToString(byteArray, Base64.NO_WRAP);
            BitmapHolder.getInstance().setSetLiveNessImageEncoded(encoded);
            return encoded;
        } catch (Exception e) {
            e.printStackTrace();
            return "n/a";
        }

    }

/*
    private void initializeClassifier() {
        try {
            detector = TFLiteObjectDetectionAPIModel.create(getAssets(),
                    TF_OD_API_MODEL_FILE,
                    TF_OD_API_LABELS_FILE,
                    TF_OD_API_INPUT_SIZE,
                    TF_OD_API_IS_QUANTIZED);

        }catch (Exception exception){
            Log.d(TAG, "initializeClassifier: "+ exception.getLocalizedMessage());
            Toast.makeText(this, "Unable to load model", Toast.LENGTH_SHORT).show();
        }
    }
*/
}