package com.iserveu.isuekyc.liveness;

public class FaceLivenessModelISUeKyc {
    private static FaceLivenessModelISUeKyc faceLivenessModelISUeKyc;
    private String faceLivenessImageEncoded;
    public static FaceLivenessModelISUeKyc getInstance(){
        if (faceLivenessModelISUeKyc==null){
            faceLivenessModelISUeKyc = new FaceLivenessModelISUeKyc();
        }
        return faceLivenessModelISUeKyc;
    }

    public String getFaceLivenessImageEncoded() {
        return faceLivenessImageEncoded;
    }

    public FaceLivenessModelISUeKyc setFaceLivenessImageEncoded(String faceLivenessImageEncoded) {
        this.faceLivenessImageEncoded = faceLivenessImageEncoded;
        return faceLivenessModelISUeKyc;
    }
}
