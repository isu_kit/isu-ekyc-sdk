package com.iserveu.isuekyc.liveness;

public interface MovementListener {
    public void isMovementDetected(boolean isDetected);
}
