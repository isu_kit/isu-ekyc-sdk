package com.iserveu.isuekyc.digital_signature;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.latin.TextRecognizerOptions;
import com.iserveu.isuekyc.utils.Constants;
import com.iserveu.isuekyc.utils.FullImageView;

import java.io.FileOutputStream;
import java.util.List;

public class DigitalSignatureDetectionProcessor {
    private static final String TAG = DigitalSignatureDetectionProcessor.class.getSimpleName();
    private static DigitalSignatureDetectionProcessor digitalSignatureDetectionProcessor;
    private Context context;

    public static DigitalSignatureDetectionProcessor getInstance(){
        if (digitalSignatureDetectionProcessor == null){
            digitalSignatureDetectionProcessor = new DigitalSignatureDetectionProcessor();
        }
        return digitalSignatureDetectionProcessor;
    }

    public void processImage(Context context, Bitmap imageBitmap)  {
        this.context = context;
        InputImage image = InputImage.fromBitmap(imageBitmap,0);
        TextRecognizer recognizer = TextRecognition.getClient( TextRecognizerOptions.DEFAULT_OPTIONS);
        recognizer.process(image).addOnSuccessListener(new OnSuccessListener<Text>() {
            @Override
            public void onSuccess(Text text) {
                detectSignature(text,imageBitmap);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        }).addOnCompleteListener(new OnCompleteListener<Text>() {
            @Override
            public void onComplete(@NonNull Task<Text> task) {

            }
        });


    }

    private void detectSignature(Text text,Bitmap bitmap) {
        boolean isSignatureAvailable = false;
        Log.d(TAG, "detectSignature: complete_text "+text.getText());
        List<Text.TextBlock> blocks = text.getTextBlocks();

        int i=0;
        while (i<blocks.size()){

            if (isSignatureAvailable){
                Log.d(TAG, "detectSignature: ");
            }else{
                if (blocks.get(i).getText().contains("Sign") || blocks.get(i).getText().contains("/ Sign") || blocks.get(i).getText().contains("ature")){
                    Log.d(TAG, "detectSignature: "+blocks.get(i).getText());
                    isSignatureAvailable = true;
                    Rect bounds = blocks.get(i).getBoundingBox();
                    if (bounds!=null){
                        saveDetectedSignature(bounds,bitmap);
                    }
                 }
            }
            i++;
        }

        if (!isSignatureAvailable){
            Toast.makeText(context, "Signature not found", Toast.LENGTH_SHORT).show();
//            extractSignatureBitmap();
        }
    }

    private void saveDetectedSignature(Rect bounds, Bitmap bitmap) {
        Log.d(TAG, "saveDetectedSignature: boundsDetails ->  left :: "+(bounds.left) +" top :: "+(bounds.top-100) + "height :: "+bounds.height()+" width :: "+bounds.width());
        try {
            Bitmap croppedBitmap = Bitmap.createBitmap(bitmap,bounds.left-50, bounds.top-250, bounds.width()+350, bounds.height()+150);

            //Write file
            String filename = "detected_signature.png";
            FileOutputStream stream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            croppedBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            //Cleanup
            stream.close();
            croppedBitmap.recycle();

            //Pop intent
            Intent in1 = new Intent(context, FullImageView.class);
            in1.putExtra(Constants.DETECTED_SIGNATURE_FROM_DOCUMENT, filename);
            context.startActivity(in1);
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Please put full image inside camera view ", Toast.LENGTH_SHORT).show();
        }
    }

}
