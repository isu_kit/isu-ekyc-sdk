package com.iserveu.isuekyc.face_detection;

import android.util.Log;

public interface CapturedRecognition {
    /** An immutable result returned by a Classifier describing what was recognized. */
    class Recognition {
        /**
         * A unique identifier for what has been recognized. Specific to the class, not the instance of
         * the object.
         */
        private  String id;
        /** Display name for the recognition. */
        private  String title;


        private  Float distance;
        private Object extra;
        private Object savedExtra;

        private static CapturedRecognition.Recognition recognition;
        public static CapturedRecognition.Recognition getInstance(){
            if (recognition==null){
                recognition = new CapturedRecognition.Recognition();
            }
            return recognition;
        }

        public Recognition() {


        }
        public CapturedRecognition.Recognition initClassifier(final String id, final String title, final Float distance){
            this.id = id;
            this.title = title;
            this.distance = distance;
            this.extra = null;
            this.savedExtra = null;
            return recognition;
        }
        /*public Recognition(
                final String id, final String title, final Float distance) {
            this.id = id;
            this.title = title;
            this.distance = distance;
            this.extra = null;

        }*/

        public void setExtra(Object extra) {
            Log.d("Extra Object", "setExtra: "+extra);
            this.extra = extra;
        }
        public Object getExtra() {
            return this.extra;
        }

        public Object getLiveExtra() {
            return savedExtra;
        }

        public void setLiveExtra(Object savedExtra) {
            Log.d("Saved Extra Object", "setSavedExtra: "+savedExtra.toString());
            this.savedExtra = savedExtra;
        }

        @Override
        public String toString() {
            String resultString = "";
            if (id != null) {
                resultString += "[" + id + "] ";
            }

            if (title != null) {
                resultString += title + " ";
            }

            if (distance != null) {
                resultString += String.format("(%.1f%%) ", distance * 100.0f);
            }

            return resultString.trim();
        }

    }
}
